import React from 'react';
import { FcInfo } from 'react-icons/fc';
import '../styles/sematforo.css';

function SinPostsModeracion(props) {
    return <>
        <div className='row titulo-sin-posts mb-5'>
            <h1 className='col'>Ups!<FcInfo className='fc-empty-tamanio ms-3' /></h1>
        </div>
        <div className={'alert mt-3 alert-info texto-salto-linea'} >{'Actualmente no se registran'} <b>preguntas</b> {'para moderar'}</div>
        <div className='d-flex justify-content-between'>
            <div className='row ms-0 mt-5 boton-mobile-accion'>
                <button className='btn btn-primary ml-3' onClick={() => props.volver()}>Volver</button>
            </div>
        </div>
    </>
}

export default SinPostsModeracion;