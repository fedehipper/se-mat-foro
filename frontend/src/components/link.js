import React from 'react';

function Link(props) {
    return <a href={props.link} target="_blank" rel="noopener noreferrer">
        {props.texto}
    </a>
}

export default Link;