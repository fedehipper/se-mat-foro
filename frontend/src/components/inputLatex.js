import React from 'react';
import LatexConversor from './latexConversor';
import Link from './link';
import '../styles/sematforo.css';
import { FcAbout } from 'react-icons/fc';

function InputLatex(props) {
  return <div className='form'>
    <div className='row-col'>
      <p className='instruccion-inputs mb-1'>Recordá usar $ al comienzo y al final de cada fórmula.</p>
      <textarea
        id='input-latex'
        className='form-control'
        rows={10}
        onChange={props.funcion}
        value={props.vistaPrevia} />
      <div className="invalid-feedback">{props.mensajeValidacion}</div>

      <div className='row-col mt-3'>
        <FcAbout /> <Link link='https://wiki.geogebra.org/es/C%C3%B3digo_LaTeX_para_las_f%C3%B3rmulas_m%C3%A1s_comunes' texto='¿Cómo escribo en formato LATEX?' />
      </div>
      <div className='row-col'>
        <FcAbout /> <Link link='https://www.markdownguide.org/basic-syntax/' texto='¿Cómo escribo en formato MARKDOWN?' />
      </div>

      {props.vistaPrevia && <>
        <div className="card mt-3">
          <div className="card-header fa-clickable" id="headingOne" data-bs-toggle="collapse" data-target="#collapse" aria-expanded="false" href="#collapseVistaPrevia">
            <div className="mb-0 d-flex justify-content-center">
              <p className="mb-0">Vista previa</p>
            </div>
          </div>

          <div id="collapseVistaPrevia" className="collapse show">
            <div className="card-body pb-0 ps-3 ps-lg-0 pe-3 pe-lg-0">
              <LatexConversor formula={props.vistaPrevia} />
            </div>
          </div>
        </div>
      </>}

    </div>
  </div>
}

export default InputLatex;