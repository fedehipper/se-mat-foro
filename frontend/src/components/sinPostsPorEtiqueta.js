import React from 'react';
import { FcInfo } from 'react-icons/fc';
import '../styles/sematforo.css';

function SinPostsPorEtiqueta(props) {
    return <>
        <div className='row titulo-pagina mb-5'>
            <h1 className='col'>Ups!<FcInfo className='fc-empty-tamanio ms-3' /></h1>
        </div>
        <div className={'alert mt-3 alert-info texto-salto-linea'} >
            {'Actualmente no se registran preguntas para la etiqueta '}
            <b>{props.nombreEtiqueta}</b>
            {' probá con otra, suerte!'}
        </div>
    </>
}

export default SinPostsPorEtiqueta;