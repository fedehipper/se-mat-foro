import React from 'react';
import 'katex/dist/katex.min.css';
import Markdown from './markdown';
import '../styles/sematforo.css';

function LatexConversor(props) {
    return <div className='texto-salto-linea ms-0 me-0 me-lg-3 ms-lg-3'>
        <Markdown>{props.formula}</Markdown>
    </div>
}

export default LatexConversor;


