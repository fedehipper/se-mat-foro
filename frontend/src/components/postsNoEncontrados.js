import React from 'react';
import { FcSearch } from 'react-icons/fc';
import '../styles/sematforo.css';

function PostsNoEncontrados(props) {
    return <>
        <div className='row titulo-pagina'>
            <h1 className='mb-5'>Ups!<FcSearch className='fc-empty-tamanio ms-3 ' /></h1>
        </div>
        <div className={'alert mt-3 alert-warning texto-salto-linea'} >{'Tu consulta no tuvo resultados al buscar '}<strong>{props.busqueda}</strong></div>

        <div className="col mt-5">
            <button className='btn btn-primary ml-3' onClick={() => props.volverAlHome()}>Volver</button>
        </div>
    </>
}

export default PostsNoEncontrados;