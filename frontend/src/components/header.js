import React, { useEffect, useState } from 'react';
import { FcHighPriority, FcOk, FcInfo, FcFinePrint } from 'react-icons/fc';
import {
    RiAdminFill,
    RiAdminLine,
    RiLogoutBoxRLine,
    RiNotification2Fill,
    RiNotification2Line,
    RiLogoutBoxRFill
} from 'react-icons/ri';
import { useHistory } from 'react-router';
import notificacionService from '../service/notificacionService';
import usuarioService from '../service/usuarioService';
import '../styles/sematforo.css';

function Header(props) {
    const history = useHistory();
    const [usuarioLogueado, setUsuarioLogueado] = useState('');
    const [notificaciones, setNotificaciones] = useState([]);
    const [iconoPadreNotificacion, setIconoPadreNotificacion] = useState(<RiNotification2Line />);
    const [iconoCerrarSesion, setIconoCerrarSesion] = useState(<RiLogoutBoxRLine />);

    useEffect(() => {
        buscarUsuarioLogueado();
        buscarNotificaciones();
        escucharEventoDropdownVerNotificaciones();
        escucharEventoDropdownOcultarNotificaciones();
    }, []);

    const buscarNotificaciones = async () => {
        const notificaciones = await notificacionService.buscarTodas();
        setNotificaciones(notificaciones);
    }

    const buscarUsuarioLogueado = async () => {
        const usuarioLogueado = await usuarioService.buscarUsuarioLogueado();
        setUsuarioLogueado(usuarioLogueado);
    }

    const handleKeyDown = (evento) => {
        if (evento.key === 'Enter') {
            evento.preventDefault();
            const textoIngresado = evento.target.value;
            history.push(`/posts/busqueda-contenido?valor-busqueda=${textoIngresado}`);
        }
    }

    const iconoModeracion = () => (props.esPaginaModeracion) ? <RiAdminFill /> : <RiAdminLine />;
    const cambiarIconoCerrarSesion = () => setIconoCerrarSesion(<RiLogoutBoxRFill />);

    const escucharEventoDropdownVerNotificaciones = () =>
        document
            .getElementById('menu-notifiaciones')
            .addEventListener('show.bs.dropdown', () => setIconoPadreNotificacion(<RiNotification2Fill />));

    const escucharEventoDropdownOcultarNotificaciones = () =>
        document
            .getElementById('menu-notifiaciones')
            .addEventListener('hide.bs.dropdown', () => setIconoPadreNotificacion(<RiNotification2Line />));

    const iconoNotificacion = (tipoNotificacion) => {
        if (tipoNotificacion === 'PREGUNTA_APROBADA' || tipoNotificacion === 'RESPUESTA_APROBADA') {
            return <FcOk />;
        } else if (tipoNotificacion === 'PREGUNTA_RECHAZADA' || tipoNotificacion === 'RESPUESTA_RECHAZADA') {
            return <FcHighPriority />;
        } else if (tipoNotificacion === 'PREGUNTA_EN_REVISION' || tipoNotificacion === 'RESPUESTA_EN_REVISION') {
            return <FcFinePrint />
        }
    }

    const irAModeracionPage = () => history.push('/moderacion');
    const irAMisPreguntasPage = () => history.push('/mis-posts');
    const irAHomePage = () => history.push('/');

    const eliminarNotificacion = async (notificacionId) => await notificacionService.eliminarNotificacion(notificacionId);
    const mostrarMensajeSinNotificacoines = () => <li className='ms-3 fa-clickable'>
        <span className='pe-2 mt-3'>
            <FcInfo />
        </span>
        Aún no tenés notificaciones
    </li>

    const cantidadNotificacionesMobile = (cantidadNotificaciones) => cantidadNotificaciones > 0 ? '(' + cantidadNotificaciones + ')' : '';

    return <>
        <nav className='navbar navbar-expand-lg navbar-light color-menu-header fixed-top sombra'>
            <div className='container d-flex justify-content-between ps-0 pe-0'>
                <div className='row'>
                    <span className='navbar-brand fa-clickable ms-3 ms-lg-0' onClick={irAHomePage}>
                        <img alt='' src='/fonts/logo.jpg' width='30' height='33' className='d-inline-block align-top me-1' /><span className='ocultar-mobile texto-menu'></span>
                        <img alt='' src='/fonts/texto.svg' width='80' className='d-inline-block align-top' /><span className='ocultar-mobile texto-menu'></span>
                    </span>
                </div>

                <button className="navbar-toggler sin-borde" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <form className='d-flex buscador'>
                        <input className='form-control me-3 ms-3 ms-lg-0 mt-lg-0 mt-1 mb-3 mb-lg-0' type='search' placeholder='Buscar...' aria-label='Search' onKeyDown={handleKeyDown} />
                    </form>
                    <ul className='navbar-nav mb-2 mb-lg-0'>
                        {usuarioLogueado.rol === 'MODERADOR' &&
                            <li className='nav-item fa-clickable ms-3 ms-lg-0' onClick={irAModeracionPage} title='moderación'>
                                <span className='navbar-brand active' aria-current='page'>{iconoModeracion()}
                                    <span className='ocultar-desktop texto-menu'> Moderación</span>
                                </span>
                            </li>}


                        <li className='nav-item fa-clickable ms-3 ms-lg-0 mt-2 mt-lg-0' title={usuarioLogueado.nombreCompleto}>
                            <span className='navbar-brand active' aria-current='page' onClick={irAMisPreguntasPage}>
                                <img alt='' src='/fonts/perfil.jpg' width='25' height='25' className='mt-1 d-inline-block align-top' />
                                <span className='ocultar-desktop texto-menu'> Mi perfil</span>
                            </span>
                        </li>

                        <div className='dropdown'>
                            <li className='nav-item ms-3 ms-lg-0 mt-2 mt-lg-0' title='mis notificaciones' id='menu-notifiaciones' data-bs-toggle='dropdown' >
                                <span className='btn navbar-brand active position-relative p-0'>
                                    {iconoPadreNotificacion}
                                    <span className='position-absolute ocultar-mobile mt-1 pt-1 pb-1 pe-2 ps-2 translate-middle badge rounded-pill bg-danger'>
                                        {notificaciones.length > 0 ? notificaciones.length : ''}
                                    </span>
                                    <span className='ocultar-desktop texto-menu '>{` Notificaciones ${cantidadNotificacionesMobile(notificaciones.length)}`}  </span>
                                </span>
                            </li>
                            <ul className='dropdown-menu tamanio-notificaciones dropdown-menu-light boton-mobile-accion notificacion-ancho sombra' aria-labelledby='menu-notifiaciones'>
                                {notificaciones.length === 0 ? mostrarMensajeSinNotificacoines() : notificaciones.map((notificacion, index) => {
                                    return <li key={index} >
                                        <a className='dropdown-item notificacion-elipsis' href={`/posts/${notificacion.postId}`} onClick={() => eliminarNotificacion(notificacion.id)}>
                                            <span className='pe-2 mt-3'>{iconoNotificacion(notificacion.tipo)}</span>
                                            {notificacion.mensaje}
                                            <br></br>
                                            <b className='ms-4'>{notificacion.info}</b>
                                        </a>
                                        {index + 1 < notificaciones.length && <hr className='mt-2 mb-2' />}
                                    </li>
                                })}
                            </ul>
                        </div>

                        <li className='nav-item ms-3 ms-lg-0 mt-2 mt-lg-0' title='cerrar sesión' onClick={cambiarIconoCerrarSesion}>
                            <a className='navbar-brand active texto-menu' aria-current='page' href='/logout'>{iconoCerrarSesion}
                                <span className='ocultar-desktop texto-menu'> Cerrar sesión</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </>

}


export default Header;
