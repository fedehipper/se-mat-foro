import LatexConversor from "./latexConversor";
import React from 'react';

function Respuestas(props) {
    return <div className='mt-5'>
        {props.respuestas.length === 0 ? <div className='row me-0 ms-0'>
            <div className='alert alert-info mb-0'>
                Aún no han respondido esta pregunta, sé el primero!
            </div>
        </div> :
            <>
                <h4 className='mb-4'>Respuestas</h4>
                <hr />
            </>}

        {props.respuestas.map((respuesta, index) => {
            return <div key={index}>
                <LatexConversor formula={respuesta.texto} />
                <hr />
            </div>
        })}
    </div>
}

export default Respuestas;