import '../styles/sematforo.css';
import "@pathofdev/react-tag-input/build/index.css";
import ReactTagInput from "@pathofdev/react-tag-input";
import React, { useEffect } from 'react';

function Etiqueta(props) {
    useEffect(() => {
        const input = document.getElementsByClassName('react-tag-input')[0];
        if (props.validacionError) {
            input.classList.add('is-invalid');
        } else {
            input.classList.remove('is-invalid');
        }
    });

    return <>
        <label>Etiquetas</label>
        <ReactTagInput
            tags={props.etiquetas}
            onChange={nuevasEtiquetas => props.setEtiquetas(nuevasEtiquetas)}
            placeholder="Escribí y toca enter para crear tu etiqueta."
            maxTags={5}
            editable={false}
            readOnly={false}
            removeOnBackspace={true}
            validator={value => !props.etiquetas.includes(value)}
        />
        {props.validacionError && <span className='invalid-feedback'>Al menos debes escribir una etiqueta</span>}
        <p className='instruccion-inputs'>Tenes como máximo 5 etiquetas distintas para describir tu post</p>
    </>
}

export default Etiqueta;