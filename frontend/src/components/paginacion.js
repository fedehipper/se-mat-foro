import React from 'react';
import '../styles/sematforo.css';

function Paginacion(props) {
    const NUMERO_PAGINAS_VISIBLES = 5;

    const generarItems = (inicio, fin) => {
        return Array(fin - inicio + 1)
            .fill()
            .map((_, idx) => inicio + idx)
            .map(l => <li className={`page-item ${l === props.page ? 'active' : null}`} key={l}>
                <span className="page-link clickeable" onClick={() => props.actualizarNumeroPagina(l)}>{l}</span>
            </li>);
    }

    const anterior = () => {
        return <li className="page-item" onClick={() => props.actualizarNumeroPagina(props.page - 1)}>
            <span className="page-link clickeable">Anterior</span>
        </li>
    }

    const siguiente = () => {
        return <li className="page-item" onClick={() => props.actualizarNumeroPagina(props.page + 1)}>
            <span className="page-link clickeable">Siguiente</span>
        </li>
    }


    const anteriorSiSuperaPrimerPagina = () => props.page > 1 && anterior();
    const siguienteSiEsMenorAUltimaPagina = () => props.page < props.cantidadPaginas && siguiente();

    const paginaActualAlComienzoConCantidadPaginasMenorALasVisibles = () => {
        return props.page < NUMERO_PAGINAS_VISIBLES && props.cantidadPaginas <= NUMERO_PAGINAS_VISIBLES;
    }

    const paginaActualAlComienzoConCantidadPaginasSuperandoLasVisibles = () => {
        return props.page < NUMERO_PAGINAS_VISIBLES && props.cantidadPaginas > NUMERO_PAGINAS_VISIBLES;
    }

    const paginaActualEnRangoMedio = () => {
        return props.page >= NUMERO_PAGINAS_VISIBLES && (props.page + 2) < props.cantidadPaginas;
    }

    const elipsis = () => <li className="page-item"><span className="page-link clickeable">...</span></li>

    const renderizarItemsPaginas = () => {
        if (paginaActualAlComienzoConCantidadPaginasMenorALasVisibles()) {
            return <ul className="pagination mb-3">
                {anteriorSiSuperaPrimerPagina()}
                {generarItems(1, props.cantidadPaginas)}
                {props.page < props.cantidadPaginas && siguiente()}
            </ul>
        } else if (paginaActualAlComienzoConCantidadPaginasSuperandoLasVisibles()) {
            return <ul className="pagination mb-3">
                {anteriorSiSuperaPrimerPagina()}
                {generarItems(1, NUMERO_PAGINAS_VISIBLES)}
                {elipsis()}
                <li
                    className="page-item"
                    onClick={() => props.actualizarNumeroPagina(props.cantidadPaginas)}>
                    <span className="page-link clickeable">{props.cantidadPaginas}</span>
                </li>
                {siguiente()}
            </ul>
        } else if (paginaActualEnRangoMedio()) {
            return <ul className="pagination mb-3">
                {anterior()}
                <li
                    className="page-item"
                    onClick={() => props.actualizarNumeroPagina(1)}>
                    <span className="page-link clickeable">{1}</span>
                </li>
                {elipsis()}
                {generarItems(props.page - 2, props.page + 2)}
                {elipsis()}
                <li
                    className="page-item"
                    onClick={() => props.actualizarNumeroPagina(props.cantidadPaginas)}>
                    <span className="page-link clickeable">{props.cantidadPaginas}</span>
                </li>
                {siguiente()}
            </ul>
        } else {
            return <ul className="pagination mb-3">
                {anterior()}
                <li
                    className="page-item"
                    onClick={() => props.actualizarNumeroPagina(1)}>
                    <span className="page-link clickeable">{1}</span>
                </li>
                {elipsis()}
                {generarItems(props.cantidadPaginas - NUMERO_PAGINAS_VISIBLES + 1, props.cantidadPaginas)}
                {siguienteSiEsMenorAUltimaPagina()}
            </ul>
        }
    }

    return <>{renderizarItemsPaginas()}</>
}

export default Paginacion;