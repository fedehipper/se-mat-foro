import React from 'react';
import { FcInfo } from 'react-icons/fc';
import '../styles/sematforo.css';

function SinPostsRealizados(props) {
    return <>
        <div className='row titulo-sin-posts mb-5'>
            <h1 className='col'>Ups!<FcInfo className='fc-empty-tamanio ms-3' /></h1>
        </div>
        <div className={'alert mt-3 alert-info texto-salto-linea'} >{'Todavía no realizaste ninguna pregunta'}</div>
        <div className='col mt-5'>
            <button className='btn btn-primary ml-3' onClick={() => props.crearPost()}>Crea tu primer pregunta</button>
        </div>
    </>
}

export default SinPostsRealizados;