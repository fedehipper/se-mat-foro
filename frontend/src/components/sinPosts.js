import React from 'react';
import { FcInfo } from 'react-icons/fc';
import '../styles/sematforo.css';

function SinPosts(props) {
    return <>
        <div className='row titulo-pagina mb-5'>
            <h1 className='col'>Ups!<FcInfo className='fc-empty-tamanio ms-3' /></h1>
        </div>
        <div className={'alert mt-3 alert-info texto-salto-linea'} >{'Actualmente no se registran preguntas para visualizar'}</div>
        <div className='col mt-5'>
            <button className='btn btn-primary ml-3 boton-mobile-accion' onClick={() => props.crearPost()}>Crea la primer pregunta</button>
        </div>
    </>
}

export default SinPosts;