import React from 'react';
import { FcSearch } from 'react-icons/fc';
import '../styles/sematforo.css';

function BusquedaInvalida(props) {
    return <>
        <div className='row titulo-pagina'>
            <h1 className='mb-5'>Ups!<FcSearch className='fc-empty-tamanio ms-3 ' /></h1>
        </div>
        <div className={'alert mt-3 alert-danger texto-salto-linea'} >{'Es necesario que la búsqueda contenga un texto.'}</div>

        <div className="row mt-5">
            <div className='col d-flex justify-content-end'>
                <button className='btn btn-primary ml-3' onClick={() => props.volverAlHome()}>Volver</button>
            </div>
        </div>
    </>
}

export default BusquedaInvalida;