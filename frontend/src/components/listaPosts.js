import { useHistory } from "react-router";
import '../styles/sematforo.css';
import React from 'react';
import postUtils from "../utils/postUtils";

function ListaPosts(props) {

    const history = useHistory();
    const irAlPost = (post) =>
        props.esModeracion ? history.push({ pathname: `/moderacion/posts/${post.id}` }) : history.push({ pathname: `/posts/${post.id}` });

    const irABusquedaPorEtiqueta = (etiqueta) => history.push({ pathname: `/posts/busqueda-etiqueta/${etiqueta}`, state: etiqueta });

    const estado = (estado) => {
        return props.visualizarEstado
            && postUtils.estadoPost(estado)
            && <div className={`alert ${estado === 'PENDIENTE_DE_APROBACION' ? 'alert-warning' : 'alert-danger'}`}>
                {postUtils.estadoPost(estado)}
            </div>
    }

    const renderPost = (post) => {
        return <div key={post.id}>
            <hr />
            <div className='ms-0 ms-lg-3 mb-3'>
                <div className='row'>
                    <h4 onClick={() => irAlPost(post)} className='col mb-4 fa-clickable text-primary nounderline texto-salto-linea'>{post.titulo}</h4>
                </div>
                {estado(post.estado)}

                {post.etiquetas.map((etiqueta, index) => <span
                    key={index}
                    className='badge me-2 tag-descripcion'
                    onClick={() => irABusquedaPorEtiqueta(etiqueta)}>{etiqueta}
                </span>)}

            </div>
        </div>
    }

    return <>
        {props.posts.map(post => renderPost(post))}
    </>
}

export default ListaPosts;