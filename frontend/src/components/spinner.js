import React from 'react';
import '../styles/sematforo.css';

function Spinner() {
    return <div className='text-center posicion-spinner d-flex flex-column align-items-center justify-content-end'>
        <div className='justify-content-between'>
            <div className='spinner-border spinner text-success' role='status' />
            <div className='mt-1'>
                <span className='text-success'>Cargando...</span>
            </div>
        </div>
    </div>
}

export default Spinner;