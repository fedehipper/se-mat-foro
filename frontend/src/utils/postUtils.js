const postUtils = {

    estadoPost: (estado) => {
        switch (estado) {
            case 'PENDIENTE_DE_APROBACION': return 'Pendiente de aprobación, es necesario que un moderador apruebe tu pregunta.';
            case 'RECHAZADO': return 'Rechazado, mira el motivo de rechazo para editar tu post correctamente.';
            default: return '';
        }
    }

}

export default postUtils;