import axios from "axios";

const etiquetaService = {
    actualizarEtiquetas: async (etiquetas) => {
        const response = await axios.post(`/api/etiquetas`, etiquetas);
        return response.data;
    },

    eliminarPorPostId: (postId) => {
        return axios.delete(`/api/etiquetas?postId=${postId}`);
    }
};

export default etiquetaService;