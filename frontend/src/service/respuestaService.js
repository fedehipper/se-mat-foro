import axios from "axios"

const respuestaService = {
    responder: async (respuesta) => {
        const response = await axios.post('/api/post/respuesta', respuesta);
        return response.data;
    },

    buscarParaPost: async (postId) => {
        const response = await axios.get(`/api/post/${postId}/respuestas`);
        return response.data;
    }
}

export default respuestaService;