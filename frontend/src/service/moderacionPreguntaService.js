import axios from "axios";

const moderacionPreguntaService = {

    buscarPostsPendientesAprobacion: async (pagina, cantidad) => {
        const response = await axios.get(`/api/posts/pendientes?page=${pagina}&size=${cantidad}`);
        return response.data;
    },

    cantidadPostsPendientesAprobacion: async () => {
        const response = await axios.get('/api/posts/pendientes/cantidad');
        return response.data;
    },

    guardar: async (postId, estadoPost, motivoRechazo) => {
        const solicitudModeracionPost = {postId, estadoPost, motivoRechazo}

        const response = await axios.put(`/api/posts/${postId}/moderacion`, solicitudModeracionPost);
        return response.data;
    }

};

export default moderacionPreguntaService;