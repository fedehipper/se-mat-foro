import axios from 'axios';

const usuarioService = {
    buscarUsuarioLogueado: async () => {
        const rol = await axios.get("/api/usuario/rol");
        return rol.data;
    }
}

export default usuarioService;