import axios from "axios";

const postService = {
    buscarTodos: async (page, size) => {
        const response = await axios.get(`/api/posts?page=${page}&size=${size}`);
        return response.data;
    },

    crearPost: async (post) => {
        const response = await axios.post('/api/post', post);
        return response.data;
    },

    editarPost: async (postId, post) => {
        const response = await axios.post(`/api/post/${postId}`, post);
        return response.data;
    },

    buscarPorId: async (postId) => {
        const response = await axios.get(`/api/posts/${postId}`);
        return response.data;
    },

    cantidadPosts: async () => {
        const response = await axios.get('/api/posts/cantidad');
        return response.data;
    },

    buscarPostsPorEtiqueta: async (etiqueta, page, size) => {
        const response = await axios.get(`/api/posts/etiqueta/${etiqueta}?page=${page}&size=${size}`);
        return response.data;
    },

    cantidadPostsPorEtiqueta: async (etiqueta) => {
        const response = await axios.get(`/api/posts/etiqueta/cantidad?etiqueta=${etiqueta}`);
        return response.data;
    }
}

export default postService;