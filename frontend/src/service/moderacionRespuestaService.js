import axios from "axios";

const moderacionRespuestaService = {

    buscarRespuestasPendientesAprobacion: async () => {
        const response = await axios.get('/api/respuestas/pendientes');
        return response.data;
    },

    cambiarEstadoA: async (respuestaId, estado) => {
        const response = await axios.put(`/api/respuestas/${respuestaId}?estado=${estado}`);
        return response.data;
    }

}

export default moderacionRespuestaService;