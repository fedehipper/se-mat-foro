import axios from "axios"

const sesionService = {
    cerrarSesion: async () => {
        await axios.post('/logout');
    }
}

export default sesionService;