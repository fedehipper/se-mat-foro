import axios from "axios";

const misPostsService = {

    buscarTodos: async (pagina, cantidad) => {
        const response = await axios.get(`/api/usuario/posts?page=${pagina}&size=${cantidad}`);
        return response.data;
    },

    cantidadMisPosts: async () => {
        const response = await axios.get('/api/usuario/posts/cantidad');
        return response.data;
    }

}

export default misPostsService;