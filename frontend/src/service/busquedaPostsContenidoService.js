import axios from "axios"

const busquedaPostsContenidoService = {

    buscarPorContenido: async (busqueda, page, size) => {
        const response = await axios.get(`/api/posts/contenido?busqueda=${busqueda}&page=${page}&size=${size}`)
        return response.data;
    },

    cantidadPosts: async (busqueda) => {
        const response = await axios.get(`/api/posts/contenido/cantidad?busqueda=${busqueda}`);
        return response.data;
    }

}

export default busquedaPostsContenidoService;

