import axios from "axios";

const notificacionService = {

    guardar: async (solicitudNotificacion) => {
        const response = await axios.post('/api/notificacion', solicitudNotificacion);
        return response.data;
    },

    buscarTodas: async () => {
        const response = await axios.get('/api/notificaciones');
        return response.data;
    },

    eliminarNotificacion: async (notificacionId) => {
        await axios.delete(`/api/notificaciones/${notificacionId}`);
    }

}

export default notificacionService;