import { useEffect, useRef, useState } from "react";
import { useHistory, useLocation } from "react-router";
import InputLatex from "../components/inputLatex";
import postService from "../service/postService";
import '../styles/sematforo.css';
import React from 'react';
import Etiqueta from "../components/etiqueta";
import Header from '../components/header';
import etiquetaService from "../service/etiquetaService";
import { FcEditImage } from 'react-icons/fc';
import Spinner from '../components/spinner';
import notificacionService from "../service/notificacionService";

function EdicionPostPage() {
  const location = useLocation();
  const history = useHistory();
  const [post, setPost] = useState({});
  const [consultaIngresadaFormatoLatex, setConsultaIngresadaFormatoLatex] = useState('');
  const [titulo, setTitulo] = useState('');
  const [etiquetas, setEtiquetas] = useState([]);
  const [verSpinner, setVerSpinner] = useState(false);
  const mountedRef = useRef(true);
  const [mensajeValidacionTitulo, setMensajeValidacionTitulo] = useState('');
  const [mensajeValidacionFormatoLatex, setMensajeValidacionFormatoLatex] = useState('');
  const [esErrorEtiqueta, setEsErrorEtiqueta] = useState(false);

  useEffect(() => {
    buscarPost();
    return () => mountedRef.current = false;
    // eslint-disable-next-line
  }, []);

  const asignarConsultaIngresadaFormatoLatex = (evento) => setConsultaIngresadaFormatoLatex(evento.target.value);

  const buscarPost = async () => {
    setVerSpinner(true);
    const postBuscado = await postService.buscarPorId(location.pathname.replace('/edicion/', ''));
    setTitulo(postBuscado.titulo);
    setEtiquetas(postBuscado.etiquetas);
    setConsultaIngresadaFormatoLatex(postBuscado.contenido);
    setPost(postBuscado);
    setVerSpinner(false);
  }

  const editarPost = async () => {
    if (validarInputs()) {
      setVerSpinner(true);
      const contenido = consultaIngresadaFormatoLatex;
      const postAEditar = { contenido, titulo, etiquetas }
      try {
        const postEditado = await postService.editarPost(post.id, postAEditar);

        await etiquetaService.eliminarPorPostId(postEditado.id);

        const solicitudEtiquetas = crearSolicitudEtiquetas(etiquetas, postEditado.id);
        await etiquetaService.actualizarEtiquetas(solicitudEtiquetas);
        guardarNotificacion(postEditado);
        irAPostEditado(postEditado.id);
      } catch (error) {
        setVerSpinner(false);
      }
      setVerSpinner(false);
    }
  }

  const guardarNotificacion = async (postEditado) => {
    const tipoNotificacion = 'PREGUNTA_EN_REVISION';
    const notificacion = {
      tipo: tipoNotificacion,
      contenidoId: postEditado.id,
      tipoContenido: 'PREGUNTA',
      usuarioId: postEditado.usuarioId,
      info: postEditado.titulo
    }
    await notificacionService.guardar(notificacion);
  }

  const crearSolicitudEtiquetas = (etiquetas, postId) => {
    return etiquetas
      .map(etiqueta => {
        return { etiqueta: etiqueta, postId: postId }
      });
  }

  const handleKeyDown = (event) => (event.key === 'Enter') ?? event.preventDefault();

  const asignarTitulo = (evento) => setTitulo(evento.target.value);
  const irAPostEditado = (postId) => history.push({ pathname: `/post/success/${postId}`, state: { postId: postId } });
  const volverAPostPage = () => history.push(`/posts/${post.id}`);

  const validarInputTitulo = () => {
    const input = document.getElementById('input-titulo');
    if (!titulo.trim()) {
      setMensajeValidacionTitulo('El título es requerido');
      input.classList.add('is-invalid');
      return false;
    } else {
      setMensajeValidacionTitulo('');
      input.classList.remove('is-invalid');
      return true;
    }
  }

  const validarEtiquetas = () => {
    if (!etiquetas.length) {
      setEsErrorEtiqueta(true);
      return false;
    } else {
      setEsErrorEtiqueta(false);
      return true;
    }
  }

  const validarInputCuerpoFormatoLatex = () => {
    const input = document.getElementById('input-latex');
    if (!consultaIngresadaFormatoLatex.trim()) {
      setMensajeValidacionFormatoLatex('El cuerpo es requerido');
      input.classList.add('is-invalid');
      return false;
    } else if (consultaIngresadaFormatoLatex.trim().length < 30) {
      setMensajeValidacionFormatoLatex('El cuerpo debe tener como mínimo 30 caracteres');
      input.classList.add('is-invalid');
      return false;
    } else {
      setMensajeValidacionFormatoLatex('');
      input.classList.remove('is-invalid');
      return true;
    }
  }

  const validarInputs = () => {
    return ![validarInputTitulo, validarInputCuerpoFormatoLatex, validarEtiquetas]
      .map(validacion => validacion())
      .includes(false);
  }

  return <>
    <Header />
    {verSpinner ? <Spinner /> : <>
      <div className='row titulo-pagina'>
        <h2 className='mb-4'>Editá tu pregunta<FcEditImage className='fc-success-tamanio ms-3 ' /></h2>
      </div>

      <div className='form mt-3'>
        <label>Título</label>
        <input id='input-titulo' className='form-control' onChange={asignarTitulo} defaultValue={post.titulo} onKeyDown={handleKeyDown} />
        <div className="invalid-feedback">{mensajeValidacionTitulo}</div>

        <div className='mt-3'>
          <label>Cuerpo</label>
          <InputLatex
            funcion={asignarConsultaIngresadaFormatoLatex}
            vistaPrevia={consultaIngresadaFormatoLatex}
            mensajeValidacion={mensajeValidacionFormatoLatex} />
        </div>

        <div className='mt-3'>
          <Etiqueta setEtiquetas={setEtiquetas} etiquetas={etiquetas} validacionError={esErrorEtiqueta} />
        </div>

        <div className="d-flex justify-content-between mt-5 pb-3">
          <div className='row me-2 ms-0 boton-mobile-accion'>
            <button className='btn btn-primary' onClick={volverAPostPage}>Volver</button>
          </div>
          <div className='row me-0 ms-2 boton-mobile-accion'>
            <button onClick={editarPost} className='btn btn-success'>Guardar</button>
          </div>
        </div>

      </div>
    </>}
  </>
}

export default EdicionPostPage;