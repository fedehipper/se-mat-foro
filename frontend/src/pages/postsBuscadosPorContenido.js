import React, { useEffect, useState } from 'react';
import '../styles/sematforo.css';
import Header from '../components/header';
import busquedaPostsContenidoService from '../service/busquedaPostsContenidoService';
import { useLocation, useHistory } from "react-router";
import PostsNoEncontrados from '../components/postsNoEncontrados';
import ListaPosts from '../components/listaPosts';
import Paginacion from '../components/paginacion';
import Spinner from '../components/spinner';
import BusquedaInvalida from '../components/busquedaInvalida';

function PostsBuscadosPorContenido() {

    const location = useLocation();
    const history = useHistory();
    const [posts, setPosts] = useState([]);
    const [busqueda, setBusqueda] = useState('');
    const [numeroPagina, setNumeroPagina] = useState(1);
    const [cantidadPaginas, setCantidadPaginas] = useState(0);
    const postsPorPagina = 10;
    const [verSpinner, setVerSpinner] = useState(false);

    useEffect(() => {
        buscarPorContenido();
        // eslint-disable-next-line
    }, [location]);

    const volverAlHome = () => history.push('/');

    const buscarPorContenido = async () => {
        setVerSpinner(true);

        const search = location.search;
        const busqueda = new URLSearchParams(search).get('valor-busqueda');

        const cantidadPosts = await busquedaPostsContenidoService.cantidadPosts(busqueda);
        setCantidadPaginas(Math.ceil(cantidadPosts / postsPorPagina));

        const postsEncontrados = await busquedaPostsContenidoService.buscarPorContenido(busqueda, 0, 10);

        setPosts(postsEncontrados);
        setBusqueda(busqueda);
        setVerSpinner(false);
    }

    const postEncontrados = () => posts.length;

    const mostrarPosts = () => {
        return <>
            <div className='titulo-pagina d-flex justify-content-between'>
                <h2 className='mb-4'>Preguntas encontradas</h2>
            </div>
            <ListaPosts posts={posts} esModeracion={false} />
            {posts.length > 0 && <hr />}

            <div className='col d-flex justify-content-center mt-5'>
                <Paginacion
                    page={numeroPagina}
                    cantidadPaginas={cantidadPaginas}
                    actualizarNumeroPagina={setNumeroPagina} />
            </div>
        </>
    }

    const mostrarPostsNoEncontrados = () => <PostsNoEncontrados volverAlHome={volverAlHome} busqueda={busqueda} />
    const mostrarBusquedaInvalida = () => <BusquedaInvalida volverAlHome={volverAlHome} />

    return <>
        <Header busquedaInvalida={busqueda} />
        {busqueda ? (verSpinner ? <Spinner /> : (postEncontrados() ? mostrarPosts() : mostrarPostsNoEncontrados())) : mostrarBusquedaInvalida()}
    </>

}

export default PostsBuscadosPorContenido;