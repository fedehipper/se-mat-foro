import { useEffect, useRef, useState } from 'react';
import React from 'react';
import { useHistory } from 'react-router';
import InputLatex from '../components/inputLatex';
import postService from '../service/postService';
import '../styles/sematforo.css';
import Etiqueta from '../components/etiqueta';
import Header from '../components/header';
import etiquetaService from '../service/etiquetaService';
import { FcQuestions } from 'react-icons/fc';
import Spinner from '../components/spinner';
import notificacionService from '../service/notificacionService';

function Foro() {

  const [consultaIngresadaFormatoLatex, setConsultaIngresadaFormatoLatex] = useState('');
  const [titulo, setTitulo] = useState('');
  const [etiquetas, setEtiquetas] = useState([]);
  const history = useHistory();
  const [verSpinner, setVerSpinner] = useState(false);
  const mountedRef = useRef(true);
  const [mensajeValidacionTitulo, setMensajeValidacionTitulo] = useState('');
  const [mensajeValidacionFormatoLatex, setMensajeValidacionFormatoLatex] = useState('');
  const [esErrorEtiqueta, setEsErrorEtiqueta] = useState(false);

  useEffect(() => {
    return () => mountedRef.current = false;
    // eslint-disable-next-line
  }, []);

  const asignarConsultaIngresadaFormatoLatex = (evento) => setConsultaIngresadaFormatoLatex(evento.target.value);
  const asignarTitulo = (evento) => setTitulo(evento.target.value);

  const crearPost = async () => {
    if (validarInputs()) {
      setVerSpinner(true);
      const contenido = consultaIngresadaFormatoLatex;
      const post = { contenido, titulo, etiquetas }
      try {
        const postCreado = await postService.crearPost(post);
        irAPostCreado(postCreado.id);

        const solicitudEtiquetas = crearSolicitudEtiquetas(etiquetas, postCreado.id);
        await etiquetaService.actualizarEtiquetas(solicitudEtiquetas);
        guardarNotificacion(postCreado);
      } catch (error) {
        setVerSpinner(false);
      }
      setVerSpinner(false);
    }
  }

  const guardarNotificacion = async (postCreado) => {
    const tipoNotificacion = 'PREGUNTA_EN_REVISION';
    const notificacion = {
      tipo: tipoNotificacion,
      contenidoId: postCreado.id,
      tipoContenido: 'PREGUNTA',
      usuarioId: postCreado.usuarioId,
      info: postCreado.titulo
    }
    await notificacionService.guardar(notificacion);
  }

  const crearSolicitudEtiquetas = (etiquetas, postId) => {
    return etiquetas
      .map(etiqueta => {
        return { etiqueta: etiqueta, postId: postId }
      });
  }

  const irAPostCreado = (postId) => history.push({ pathname: `/post/success/${postId}`, state: { postId: postId } });
  const volverAPosts = () => history.push('/');
  const handleKeyDown = (event) => event.key === 'Enter' ?? event.preventDefault();

  const validarInputTitulo = () => {
    const input = document.getElementById('input-titulo');
    if (!titulo.trim()) {
      setMensajeValidacionTitulo('El título es requerido');
      input.classList.add('is-invalid');
      return false;
    } else {
      setMensajeValidacionTitulo('');
      input.classList.remove('is-invalid');
      return true;
    }
  }

  const validarEtiquetas = () => {
    if (!etiquetas.length) {
      setEsErrorEtiqueta(true);
      return false;
    } else {
      setEsErrorEtiqueta(false);
      return true;
    }
  }

  const validarInputCuerpoFormatoLatex = () => {
    const input = document.getElementById('input-latex');
    if (!consultaIngresadaFormatoLatex.trim()) {
      setMensajeValidacionFormatoLatex('El cuerpo es requerido');
      input.classList.add('is-invalid');
      return false;
    } else if (consultaIngresadaFormatoLatex.trim().length < 30) {
      setMensajeValidacionFormatoLatex('El cuerpo debe tener como mínimo 30 caracteres');
      input.classList.add('is-invalid');
      return false;
    } else {
      setMensajeValidacionFormatoLatex('');
      input.classList.remove('is-invalid');
      return true;
    }
  }

  const validarInputs = () => {
    return ![validarInputTitulo, validarInputCuerpoFormatoLatex, validarEtiquetas]
      .map(validacion => validacion())
      .includes(false);
  }

  return <>
    <Header />
    {verSpinner ? <Spinner /> : <>
      <div className='row titulo-pagina'>
        <h2 className='mb-4'>Escribí tu pregunta<FcQuestions className='fc-success-tamanio ms-3 ' /></h2>
      </div>

      <div className='form mt-3'>
        <label>Título</label>
        <input id='input-titulo' className='form-control' autoFocus={true} onChange={asignarTitulo} defaultValue={titulo} onKeyDown={handleKeyDown} />
        <div className="invalid-feedback">{mensajeValidacionTitulo}</div>

        <div className='mt-3'>
          <label>Cuerpo</label>
          <InputLatex
            funcion={asignarConsultaIngresadaFormatoLatex}
            vistaPrevia={consultaIngresadaFormatoLatex}
            mensajeValidacion={mensajeValidacionFormatoLatex} />
        </div>

        <div className='mt-3'>
          <Etiqueta setEtiquetas={setEtiquetas} etiquetas={etiquetas} validacionError={esErrorEtiqueta} />
        </div>

        <div className='d-flex justify-content-between mt-5 pb-3'>
          <div className='row me-2 ms-0 boton-mobile-accion'>
            <button className='btn btn-primary' onClick={volverAPosts}>Volver</button>
          </div>
          <div className='row me-0 ms-2 boton-mobile-accion'>
            <button className='btn btn-success' onClick={crearPost}>Publicar</button>
          </div>
        </div>
      </div>
    </>}
  </>
}

export default Foro;
