import { useEffect, useState } from "react";
import React from 'react';
import { useHistory, useLocation } from "react-router";
import LatexConversor from "../components/latexConversor";
import postService from "../service/postService";
import Header from '../components/header';
import Spinner from '../components/spinner';
import '../styles/sematforo.css';
import usuarioService from "../service/usuarioService";
import { GoAlert } from 'react-icons/go';
import InputLatex from "../components/inputLatex";
import respuestaService from "../service/respuestaService";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Respuestas from "../components/respuestas";
import notificacionService from "../service/notificacionService";

function Post() {
    const location = useLocation();
    const history = useHistory();
    const [respuestaIngresadaFormatoLatex, setRespuestaIngresadaFormatoLatex] = useState('');
    const [verSpinner, setVerSpinner] = useState(false);
    const [post, setPost] = useState({});
    const [usuarioLogueado, setUsuarioLogueado] = useState('');
    const [mensajeValidacionFormatoLatex, setMensajeValidacionFormatoLatex] = useState('');
    const notify = () => toast.success("Tu respuesta fue guardada con éxito!");
    const [respuestas, setRespuestas] = useState([]);

    useEffect(() => {
        buscarPost();
        buscarUsuarioLogueado();
        // eslint-disable-next-line
    }, []);

    const buscarPost = async () => {
        setVerSpinner(true);
        const postBuscado = await postService.buscarPorId(location.pathname.replace('/posts/', ''));
        setPost(postBuscado);
        buscarRespuestasParaPost(postBuscado.id);
        setVerSpinner(false);
    }

    const buscarRespuestasParaPost = async (postId) => {
        const respuestasBuscadas = await respuestaService.buscarParaPost(postId);
        setRespuestas(respuestasBuscadas);
    }

    const buscarUsuarioLogueado = async () => {
        const usuarioLogueado = await usuarioService.buscarUsuarioLogueado();
        setUsuarioLogueado(usuarioLogueado);
    }

    const asignarRespuestaIngresadaFormatoLatex = (evento) => setRespuestaIngresadaFormatoLatex(evento.target.value);

    const volver = () => history.goBack();
    const irAEdicionPostPage = (postId) => history.push({ pathname: `/edicion/${postId}` });
    const irABusquedaPorEtiqueta = (etiqueta) => history.push({ pathname: `/posts/busqueda-etiqueta/${etiqueta}`, state: etiqueta });

    const validarInputRespuestaFormatoLatex = () => {
        const input = document.getElementById('input-latex');
        if (!respuestaIngresadaFormatoLatex.trim()) {
            setMensajeValidacionFormatoLatex('El cuerpo es requerido');
            input.classList.add('is-invalid');
            return false;
        } else if (respuestaIngresadaFormatoLatex.trim().length < 30) {
            setMensajeValidacionFormatoLatex('El cuerpo debe tener como mínimo 30 caracteres');
            input.classList.add('is-invalid');
            return false;
        } else {
            setMensajeValidacionFormatoLatex('');
            input.classList.remove('is-invalid');
            return true;
        }
    }

    const responder = async () => {
        if (validarInputRespuestaFormatoLatex()) {
            setVerSpinner(true);
            try {
                const respuesta = respuestaIngresadaFormatoLatex;
                const postId = post.id;
                const body = { postId, respuesta }
                await respuestaService.responder(body);
                eliminarContenidoRespuesta();
                buscarRespuestasParaPost(post.id);
                mostrarToastPreguntaRealizadaConExito();
                guardarNotificacion()
            } catch (error) {
                setVerSpinner(false);
            }
            setVerSpinner(false);
        }
    }

    const guardarNotificacion = async () => {
        const tipoNotificacion = 'RESPUESTA_EN_REVISION';
        const notificacion = {
            tipo: tipoNotificacion,
            contenidoId: post.id,
            tipoContenido: 'RESPUESTA',
            usuarioId: post.usuarioId,
            info: post.titulo
        }
        await notificacionService.guardar(notificacion);
    }

    const mostrarToastPreguntaRealizadaConExito = () => notify();

    const eliminarContenidoRespuesta = () => setRespuestaIngresadaFormatoLatex('');

    return <>
        <Header />
        {verSpinner ? <Spinner /> : <>
            <h2 className='titulo-pagina mb-4 texto-salto-linea'>{post.titulo}</h2>
            <hr />

            <div className='texto-salto-linea'>
                <LatexConversor formula={post.contenido} />
                {post.etiquetas?.map((etiqueta, index) =>
                    <span
                        key={index}
                        className="badge me-2 tag-descripcion"
                        onClick={() => irABusquedaPorEtiqueta(etiqueta)}>{etiqueta}
                    </span>
                )}
            </div>


            {usuarioLogueado.id === post.usuarioId && post.motivoRechazo &&
                <div>
                    <div>
                        <div className='mt-5'><GoAlert className='me-1 mb-1 text-danger' />
                            <b>Editá tu pregunta</b>
                        </div>
                        <span className='texto-salto-linea'>{post.motivoRechazo}</span>
                    </div>
                </div>
            }
            {usuarioLogueado.id === post.usuarioId &&
                <div className="col mt-5 mb-5">
                    <button className='btn btn-primary boton-mobile-accion' onClick={() => irAEdicionPostPage(post.id)}>Editar</button>
                </div>
            }

            <Respuestas respuestas={respuestas} />

            <div className='mt-5'>
                <h4>Tu respuesta</h4>
                <InputLatex
                    funcion={asignarRespuestaIngresadaFormatoLatex}
                    vistaPrevia={respuestaIngresadaFormatoLatex}
                    mensajeValidacion={mensajeValidacionFormatoLatex} />
            </div>
            <div className='d-flex justify-content-between mt-5 pb-3'>
                <div className='row me-2 ms-0 boton-mobile-accion'>
                    <button className='btn btn-primary' onClick={volver}>Volver</button>
                </div>
                <div className='row me-0 ms-2 boton-mobile-accion'>
                    <button className='btn btn-success ' onClick={responder}>Responder</button>
                </div>
            </div>
        </>}
        <ToastContainer position='bottom-right' />
    </>
}

export default Post;