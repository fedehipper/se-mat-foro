import { useEffect, useState } from "react";
import React from 'react';
import { useHistory, useLocation } from "react-router";
import LatexConversor from "../components/latexConversor";
import postService from "../service/postService";
import Header from '../components/header';
import Spinner from '../components/spinner';
import '../styles/sematforo.css';
import moderacionPreguntaService from '../service/moderacionPreguntaService';
import notificacionService from '../service/notificacionService';

function PostModeracionPage() {
    const location = useLocation();
    const history = useHistory();
    const [existenRespuestas, setExistenRespuestas] = useState(false);
    const [verSpinner, setVerSpinner] = useState(false);
    const [post, setPost] = useState({});
    const [estado, setEstado] = useState('APROBADO');
    const [motivoRechazo, setMotivoRechazo] = useState('');
    const [mensajeValidacionMotivoRechazo, setMensajeValidacionMotivoRechazo] = useState('');

    useEffect(() => {
        buscarPost();
        // eslint-disable-next-line
    }, []);

    const handleChange = (evento) => setEstado(evento.target.value);
    const handleChangeMotivoRechazo = (evento) => setMotivoRechazo(evento.target.value);

    const buscarPost = async () => {
        setVerSpinner(true);
        const postBuscado = await postService.buscarPorId(location.pathname.replace('/moderacion/posts/', ''));
        setExistenRespuestas(false);
        setPost(postBuscado);
        setVerSpinner(false);
    }

    const volver = () => history.goBack();
    const irABusquedaPorEtiqueta = (etiqueta) => history.push({ pathname: `/posts/busqueda-etiqueta/${etiqueta}`, state: etiqueta });
    const guardar = async () => {
        if (validarInputMotivoRechazo()) {
            const postGuardado = await moderacionPreguntaService.guardar(post.id, estado, motivoRechazo);
            setPost(postGuardado);
            await guardarNotificacion();
            volverConPostModerado();
        }
    }

    const guardarNotificacion = async () => {
        const tipoNotificacion = estado === 'APROBADO' ? 'PREGUNTA_APROBADA' : 'PREGUNTA_RECHAZADA';
        const notificacion = {
            tipo: tipoNotificacion,
            contenidoId: post.id,
            tipoContenido: 'PREGUNTA',
            usuarioId: post.usuarioId,
            info: post.titulo
        }
        await notificacionService.guardar(notificacion);
    }

    const volverConPostModerado = () => history.push({ pathname: `/moderacion`, state: { postModerado: true } });

    const validarInputMotivoRechazo = () => {
        if (estado === 'RECHAZADO') {
            const input = document.getElementById('input-motivo-rechazo');
            if (!motivoRechazo.trim()) {
                setMensajeValidacionMotivoRechazo('El motivo de rechazo es requerido');
                input.classList.add('is-invalid');
                return false;
            } else {
                setMensajeValidacionMotivoRechazo('');
                input.classList.remove('is-invalid');
                return true;
            }
        } else {
            return true;
        }
    }

    return <>
        <Header />
        {verSpinner ? <Spinner /> : <>
            <h2 className='titulo-pagina mb-4 texto-salto-linea'>{post.titulo}</h2>
            <hr />

            <div className='ms-3 texto-salto-linea'>
                <LatexConversor formula={post.contenido}></LatexConversor>
                {post.etiquetas?.map((etiqueta, index) =>
                    <span
                        key={index}
                        className="badge me-2 tag-descripcion"
                        onClick={() => irABusquedaPorEtiqueta(etiqueta)}>{etiqueta}
                    </span>
                )}
            </div>

            <div className="row mt-5 mb-3">
                <div className='col'>
                    <label>Cambiar a estado</label>

                    <select className='form-select mb-3' onChange={handleChange}>
                        <option value='APROBADO'>Aprobado</option>
                        <option value='RECHAZADO'>Rechazado</option>
                    </select>

                    {estado === 'RECHAZADO' && <>
                        <label>Motivo de rechazo</label>
                        <input id='input-motivo-rechazo' className='form-control' onChange={handleChangeMotivoRechazo} />
                        <div className="invalid-descripcion invalid-feedback">{mensajeValidacionMotivoRechazo}</div>
                    </>}

                    <div className='d-flex justify-content-between'>
                        <div className='row me-0 ms-0 boton-mobile-accion'>
                            <button className='btn btn-primary ms-0 mt-5' onClick={volver}>Volver</button>
                        </div>
                        <div className='row me-3 ms-0 boton-mobile-accion'>
                            <button className='btn btn-success ms-3 mt-5' onClick={guardar}>Guardar</button>
                        </div>
                    </div>
                </div>
            </div>
            {existenRespuestas && <h4 className='mt-3'>Respuestas</h4>}
        </>}
    </>
}

export default PostModeracionPage;