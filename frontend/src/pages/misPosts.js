import { useEffect, useState } from "react";
import React from 'react';
import misPostsService from "../service/misPostsService";
import { useHistory } from "react-router";
import ListaPosts from '../components/listaPosts';
import Header from '../components/header';
import Paginacion from "../components/paginacion";
import Spinner from '../components/spinner';
import SinPostRealizados from "../components/sinPostsRealizados";

function MisPosts() {

    const [posts, setPosts] = useState([]);
    const [numeroPagina, setNumeroPagina] = useState(1);
    const [cantidadPaginas, setCantidadPaginas] = useState(0);
    const postsPorPagina = 10;
    const [verSpinner, setVerSpinner] = useState(false);
    const history = useHistory();

    useEffect(() => {
        buscarTodas();
        window.scrollTo(0, 0);
        // eslint-disable-next-line
    }, [numeroPagina]);

    const buscarTodas = async () => {
        setVerSpinner(true);
        const cantidadPosts = await misPostsService.cantidadMisPosts();
        setCantidadPaginas(Math.ceil(cantidadPosts / postsPorPagina));
        const posts = await misPostsService.buscarTodos(numeroPagina - 1, postsPorPagina);
        setPosts(posts);
        setVerSpinner(false);
    }

    const irACreacion = () => history.push("/creacion");

    const mostrarSinPosts = () => {
        return <SinPostRealizados crearPost={() => irACreacion()} />
    }

    const mostrarMisPosts = () => {
        return <>
            <ListaPosts visualizarEstado posts={posts} esModeracion={false} />
            {posts.length > 0 && <hr />}

            <div className='col d-flex justify-content-center mt-5'>
                <Paginacion
                    page={numeroPagina}
                    cantidadPaginas={cantidadPaginas}
                    actualizarNumeroPagina={setNumeroPagina} />
            </div>
        </>
    }

    return <>
        <Header esPaginaMisPreguntas />
        <div className='titulo-pagina d-flex justify-content-between'>
            <h2 className='mb-4'>Mis preguntas</h2>
        </div>
        <ul className='nav nav-tabs'>
            <li className='nav-item'>
                <button className='nav-link active' data-bs-toggle='tab' data-bs-target='#preguntas' type='button'>Preguntas</button>
            </li>
            {/* <li className='nav-item'>
                <button className='nav-link' data-bs-toggle='tab' data-bs-target='#respuestas' type='button'>Respuestas</button>
            </li> */}
        </ul>

        <div className='tab-content'>
            <div className='tab-pane fade show active' id='preguntas'>
                {verSpinner ? <Spinner /> : (posts.length > 0 ? mostrarMisPosts() : mostrarSinPosts())}
            </div>
            <div className='tab-pane fade' id='respuestas'>
            </div>
        </div>
    </>
}

export default MisPosts;