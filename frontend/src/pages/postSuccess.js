import { useHistory, useLocation } from "react-router";
import React from 'react';
import { FcOk } from 'react-icons/fc';
import '../styles/sematforo.css';
import Header from '../components/header';

function PostSuccess() {
    const history = useHistory();
    const location = useLocation();

    const volverAPosts = () => history.push('/');
    const irAlPost = () => history.push({ pathname: `/posts/${location.state.postId}` });

    return <>
        <Header />
        <div className='row titulo-pagina'>
            <h1 className='mb-4'>¡Felicitaciones!<FcOk className='fc-success-tamanio ms-3 ' /></h1>
        </div>
        <div className={'alert mt-3 alert-success'} >{'Tu pregunta fue guardada con éxito!'}</div>

        <div className="row mt-5">
            <div className='col'>
                <button className='btn btn-primary' onClick={volverAPosts}>Volver</button>
                <button className='btn btn-primary ms-3' onClick={irAlPost}>Ver pregunta</button>
            </div>
        </div>
    </>

}

export default PostSuccess;