import React, { useEffect, useState } from 'react';
import Header from '../components/header';
import moderacionPreguntaService from '../service/moderacionPreguntaService';
import ListaPosts from '../components/listaPosts';
import Paginacion from "../components/paginacion";
import '../styles/sematforo.css';
import { useHistory } from "react-router";
import Spinner from '../components/spinner';
import SinPostsModeracion from '../components/sinPostsModeracion';
import SinRespuestasModeracion from '../components/sinRespuestasModeracion';
import { useLocation } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import moderacionRespuestaService from '../service/moderacionRespuestaService';
import notificacionService from '../service/notificacionService';

function ModeracionPage() {

    const [posts, setPosts] = useState([]);
    const history = useHistory();
    const location = useLocation();
    const [numeroPagina, setNumeroPagina] = useState(1);
    const [cantidadPaginas, setCantidadPaginas] = useState(0);
    const postsPorPagina = 10;
    const [verSpinner, setVerSpinner] = useState(false);
    const notify = () => toast.success("Se han guardado los cambios!");
    const [respuestas, setRespuestas] = useState([]);

    useEffect(() => {
        buscarPosts(numeroPagina);
        buscarRespuestas();
        mostrarToast();
        // eslint-disable-next-line
    }, []);


    const mostrarToast = () => {
        if (location.state?.postModerado) {
            notify();
        }
    }

    const buscarPosts = async (numeroPagina) => {
        setVerSpinner(true);
        const cantidadPosts = await moderacionPreguntaService.cantidadPostsPendientesAprobacion();
        setCantidadPaginas(Math.ceil(cantidadPosts / postsPorPagina));
        const posts = await moderacionPreguntaService.buscarPostsPendientesAprobacion(numeroPagina - 1, postsPorPagina);
        setPosts(posts);
        setVerSpinner(false);
    }

    const buscarRespuestas = async () => {
        setVerSpinner(true);
        const respuestas = await moderacionRespuestaService.buscarRespuestasPendientesAprobacion();
        setRespuestas(respuestas);
        setVerSpinner(false);
    }

    const mostrarPosts = () => {
        return <>
            <ListaPosts posts={posts} esModeracion={true} />

            {posts.length > 0 && <hr />}

            <div className='col d-flex justify-content-center mt-5'>
                <Paginacion
                    page={numeroPagina}
                    cantidadPaginas={cantidadPaginas}
                    actualizarNumeroPagina={setNumeroPagina} />
            </div>
        </>
    }

    const volver = () => history.push('/');

    const mostrarSinPosts = () => <SinPostsModeracion volver={volver} />
    const mostrarSinRespuestas = () => <SinRespuestasModeracion volver={volver} />

    const handleChange = async (respuesta, evento) => {
        const estado = evento.target.value;
        if (estado) {
            const respuestaConNuevoEstado = await moderacionRespuestaService.cambiarEstadoA(respuesta.id, evento.target.value);
            notify();
            buscarRespuestas();
            guardarNotificacion(respuestaConNuevoEstado);
        }
    };

    const guardarNotificacion = async (respuestaConNuevoEstado) => {
        const tipoNotificacion = respuestaConNuevoEstado.estado === 'APROBADO' ? 'RESPUESTA_APROBADA' : 'RESPUESTA_RECHAZADA';
        const notificacion = {
            tipo: tipoNotificacion,
            contenidoId: respuestaConNuevoEstado.postId,
            tipoContenido: 'RESPUESTA',
            usuarioId: respuestaConNuevoEstado.usuarioId,
            info: respuestaConNuevoEstado.texto
        }
        console.log(notificacion)
        await notificacionService.guardar(notificacion);
    }

    const mostrarRespuestas = () => {
        return <>
            <hr />
            {respuestas.map((respuesta, index) => {
                return <div className='mt-3 texto-salto-linea' key={index}>
                    {respuesta.texto}
                    <div className='mt-3'>
                        <label>Cambiar a estado</label>
                        <select className="form-select" onChange={evento => handleChange(respuesta, evento)}>
                            <option value=''>Selecciona un estado</option>
                            <option value='APROBADO'>Aprobado</option>
                            <option value='RECHAZADO'>Rechazado</option>
                        </select>
                    </div>
                    <hr />
                </div>
            })}
        </>
    }

    return <>
        <Header esPaginaModeracion />
        <div className='titulo-pagina d-flex justify-content-between'>
            <h2 className='mb-4'>Moderación</h2>
        </div>
        <ul className='nav nav-tabs'>
            <li className='nav-item'>
                <button className='nav-link active' data-bs-toggle='tab' data-bs-target='#preguntas' type='button'>Preguntas</button>
            </li>
            <li className='nav-item'>
                <button className='nav-link' data-bs-toggle='tab' data-bs-target='#respuestas' type='button'>Respuestas</button>
            </li>
        </ul>

        <div className='tab-content'>
            <div className='tab-pane fade show active' id='preguntas'>
                {verSpinner ? <Spinner /> : posts.length > 0 ? mostrarPosts() : mostrarSinPosts()}
            </div>
            <div className='tab-pane fade' id='respuestas'>
                {verSpinner ? <Spinner /> : respuestas.length > 0 ? mostrarRespuestas() : mostrarSinRespuestas()}
            </div>
        </div>

        <ToastContainer position='bottom-right' />
    </>
}

export default ModeracionPage;