import { useEffect, useState } from "react";
import React from 'react';
import { useHistory } from "react-router";
import ListaPosts from '../components/listaPosts';
import postService from '../service/postService';
import Paginacion from "../components/paginacion";
import Header from '../components/header';
import Spinner from '../components/spinner';
import SinPosts from "../components/sinPosts";

function PostsPage() {
    const [posts, setPosts] = useState([]);
    const history = useHistory();
    const [numeroPagina, setNumeroPagina] = useState(1);
    const [cantidadPaginas, setCantidadPaginas] = useState(0);
    const postsPorPagina = 10;
    const [verSpinner, setVerSpinner] = useState(false);

    useEffect(() => {
        buscarPosts(numeroPagina);
        window.scrollTo(0, 0);
    }, [numeroPagina]);

    const buscarPosts = async (numeroPagina) => {
        setVerSpinner(true);
        const cantidadPosts = await postService.cantidadPosts();
        setCantidadPaginas(Math.ceil(cantidadPosts / postsPorPagina));
        const posts = await postService.buscarTodos(numeroPagina - 1, postsPorPagina);
        setPosts(posts);
        setVerSpinner(false);
    }

    const irACreacion = () => history.push("/creacion");

    const mostrarPosts = () => {
        return <>
            <div className='titulo-pagina d-flex justify-content-between'>
                <h2 className='mb-4'>Todas las preguntas</h2>
                <div className='boton-nueva-pregunta contenedor-mobile-accion'>
                    <button className='btn btn-primary boton-mobile-accion' onClick={irACreacion}>Escribí tu pregunta</button>
                </div>
            </div>
            <ListaPosts posts={posts} esModeracion={false} />

            {posts.length > 0 && <hr />}

            <div className='col d-flex justify-content-center mt-5'>
                <Paginacion
                    page={numeroPagina}
                    cantidadPaginas={cantidadPaginas}
                    actualizarNumeroPagina={setNumeroPagina} />
            </div>
        </>
    }

    const mostrarSinPosts = () => <SinPosts crearPost={() => irACreacion()} />

    return <>
        <Header />
        {verSpinner ? <Spinner /> : (posts.length > 0 ? mostrarPosts() : mostrarSinPosts())}
    </>
}

export default PostsPage;