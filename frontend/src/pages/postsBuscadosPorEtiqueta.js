import React from 'react';
import '../styles/sematforo.css';
import Header from '../components/header';
import { useState } from 'react';
import { useEffect } from 'react';
import postService from '../service/postService';
import ListaPosts from '../components/listaPosts';
import { useLocation } from "react-router";
import Paginacion from '../components/paginacion';
import Spinner from '../components/spinner';
import SinPostsPorEtiqueta from '../components/sinPostsPorEtiqueta';

function PostsBuscadosPorEtiqueta() {
    const [posts, setPosts] = useState([]);
    const location = useLocation();
    const [etiqueta, setEtiqueta] = useState('');
    const [cantidadPaginas, setCantidadPaginas] = useState(0);
    const [numeroPagina, setNumeroPagina] = useState(1);
    const postsPorPagina = 10;
    const [verSpinner, setVerSpinner] = useState(false);

    useEffect(() => {
        buscarPorEtiquetas(numeroPagina);
        window.scrollTo(0, 0);
        // eslint-disable-next-line
    }, [numeroPagina, location]);

    const buscarPorEtiquetas = async (numeroPagina) => {
        setVerSpinner(true);
        const nuevaEtiqueta = location.pathname.replace('/posts/busqueda-etiqueta/', '');
        (nuevaEtiqueta !== etiqueta) ?? setNumeroPagina(1);

        const cantidadPosts = await postService.cantidadPostsPorEtiqueta(nuevaEtiqueta);
        setCantidadPaginas(Math.ceil(cantidadPosts / postsPorPagina));

        const posts = await postService.buscarPostsPorEtiqueta(nuevaEtiqueta, numeroPagina - 1, postsPorPagina);
        setEtiqueta(nuevaEtiqueta);
        setPosts(posts);
        setVerSpinner(false);
    }

    const postsEncontradosPorEtiqueta = () => {
        return <>
            <div className='titulo-pagina d-flex justify-content-between'>
                <h2 className='mb-4'>Preguntas encontradas con la etiqueta [{etiqueta}]</h2>
            </div>
            <ListaPosts posts={posts} esModeracion={false} />
            <hr />
            <div className='col d-flex justify-content-center mt-5'>
                <Paginacion
                    page={numeroPagina}
                    cantidadPaginas={cantidadPaginas}
                    actualizarNumeroPagina={setNumeroPagina} />
            </div>
        </>
    }

    const postEncontrados = () => posts.length;
    const mostrarBusquedaInvalida = () => <SinPostsPorEtiqueta nombreEtiqueta={etiqueta} />

    return <>
        <Header />
        {verSpinner ? <Spinner /> : postEncontrados() ? postsEncontradosPorEtiqueta() : mostrarBusquedaInvalida()}
    </>

}

export default PostsBuscadosPorEtiqueta;