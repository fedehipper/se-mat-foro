import React from 'react';
import ReactDOM from 'react-dom';
import CreacionPostPage from './pages/creacionPostPage';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap';
import HomePage from './pages/homePage';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PostPage from './pages/postPage';
import { Component } from 'react';
import PostSuccess from './pages/postSuccess';
import EdicionPostPage from './pages/edicionPostPage';
import PostsBuscadosPorContenido from './pages/postsBuscadosPorContenido';
import PostsBuscadosPorEtiqueta from './pages/postsBuscadosPorEtiqueta';
import MisPosts from './pages/misPosts';
import ModeracionPage from './pages/moderacionPage';
import PostModeracionPage from './pages/postModeracionPage';
import { SocialIcon } from 'react-social-icons';

export class App extends Component {
    render() {
        return <>
            <div id='contenido' className='container pe-3 ps-3 ps-lg-0 fonts'>
                <Router>
                    <Switch>
                        <Route path='/' exact={true} component={HomePage} />
                        <Route path='/moderacion' exact={true} component={ModeracionPage} />
                        <Route path='/creacion' exact={true} component={CreacionPostPage} />
                        <Route path='/posts/busqueda-contenido' component={PostsBuscadosPorContenido} />
                        <Route path='/posts/busqueda-etiqueta/:etiqueta' component={PostsBuscadosPorEtiqueta} />
                        <Route path='/mis-posts' component={MisPosts} />
                        <Route path='/posts/:id' component={PostPage} />
                        <Route path='/moderacion/posts/:id' component={PostModeracionPage} />
                        <Route path='/post/success/:id' component={PostSuccess} />
                        <Route path='/edicion/:id' component={EdicionPostPage} />
                    </Switch>
                </Router>
            </div>
            <div id='footer' className='mt-3'>
                <div className='container pe-3 ps-3 ps-lg-0 fonts'>
                    <div className='margen-footer'>
                        <footer className="footer">

                            <div className='d-flex justify-content-center'>
                                <div className='icono-footer'>
                                    <SocialIcon target="_blank" rel="noopener noreferrer" url="https://www.instagram.com/sematforo/?hl=es" />
                                </div>
                            </div>
                            <div className='d-flex justify-content-center footer-texto mt-3'>
                                <span>SeMatForo © 2021</span>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        </>
    }
}

export default App;

ReactDOM.render(<App />, document.querySelector("#root"));