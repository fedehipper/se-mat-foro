package com.sematforo.service;

import com.sematforo.vo.SolicitudModeracionPostVo;
import com.sematforo.ApplicationTests;
import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.repository.PostRepository;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithUserDetails;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class ModeracionPreguntaServiceTest extends ApplicationTests {

    @Autowired
    private ModeracionPreguntaService moderacionService;

    @Autowired
    private PostRepository postRepository;

    @BeforeEach
    public void cleanDatabase() {
        postRepository.deleteAll().block();
    }

    @Test
    @WithUserDetails(value = "username")
    public void buscarPostsPendientesAprobacion_conUsuarioLogueadoRolUsuario_lanzaExcepcion() {

        Pageable pageable = PageRequest.of(0, 1);

        Flux<Post> postsEncontrados = moderacionService.buscarPostsPendientesAprobacion(pageable);

        StepVerifier
                .create(postsEncontrados)
                .expectError(AccessDeniedException.class)
                .verify();
    }

    @Test
    @WithUserDetails(value = "moderador")
    public void buscarPostsPendientesAprobacion_conUsuarioLogueadoRolModerador_retornaPostsPendientesDeAprobacion() {

        Post postPendiente = new Post();
        postPendiente.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        String postPendienteId = postRepository.save(postPendiente).block().getId();

        Post postAprobado = new Post();
        postAprobado.setEstado(EstadoContenido.APROBADO);
        postRepository.save(postAprobado).block();

        Pageable pageable = PageRequest.of(0, 2);

        Flux<Post> postsEncontrados = moderacionService.buscarPostsPendientesAprobacion(pageable);

        StepVerifier
                .create(postsEncontrados)
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(postPendienteId))
                .expectComplete()
                .verify();
    }

    @Test
    @WithUserDetails(value = "moderador")
    public void buscarPostsPendientesAprobacion_conUsuarioLogueadoRolModerador_retornaPostsPaginado() {

        Post postPendiente = new Post();
        postPendiente.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        String postPendienteId = postRepository.save(postPendiente).block().getId();

        Post postAprobado = new Post();
        postAprobado.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        postRepository.save(postAprobado).block();

        Pageable pageable = PageRequest.of(0, 1);

        Flux<Post> postsEncontrados = moderacionService.buscarPostsPendientesAprobacion(pageable);

        StepVerifier
                .create(postsEncontrados)
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(postPendienteId))
                .expectComplete()
                .verify();
    }

    @Test
    @WithUserDetails(value = "moderador")
    public void cantidadPendientesAprobacion_conUsuarioLogueadoRolModerador_retornaCantidadPostsPaginado() {

        Post postPendiente = new Post();
        postPendiente.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        postRepository.save(postPendiente).block();

        Post postAprobado = new Post();
        postAprobado.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        postRepository.save(postAprobado).block();

        Mono<Long> cantidadPostsEncontrados = moderacionService.cantidadPostsPendientesAprobacion();

        StepVerifier
                .create(cantidadPostsEncontrados)
                .assertNext(cantidad -> assertThat(cantidad).isEqualTo(2))
                .expectComplete()
                .verify();
    }

    @Test
    @WithUserDetails(value = "username")
    public void cantidadPendientesAprobacion_conUsuarioLogueadoRolUsuario_lanzaExcepcion() {

        Mono<Long> cantidad = moderacionService.cantidadPostsPendientesAprobacion();

        StepVerifier
                .create(cantidad)
                .expectError(AccessDeniedException.class)
                .verify();
    }

    @Test
    @WithUserDetails(value = "username")
    public void guardar_conUsuarioLogueadoNoModerador_lanzaExcepcion() {
        SolicitudModeracionPostVo solicitudModeracionPost = new SolicitudModeracionPostVo();
        String motivoRechazo = "Motivo rechazo con mas de sesenta caracteres para que sea valido";
        solicitudModeracionPost.setMotivoRechazo(motivoRechazo);

        Mono<Post> aprobacion = moderacionService.cambiarEstado(new SolicitudModeracionPostVo());

        StepVerifier
                .create(aprobacion)
                .expectError(AccessDeniedException.class)
                .verify();
    }

    @Test
    @WithUserDetails(value = "moderador")
    public void guardar_conUsuarioLogueadoModeradorYPostPendienteDeAprobacion_seActualizaElEstadoAAprobado() {

        Post postPendiente = new Post();
        postPendiente.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        String postId = postRepository.save(postPendiente).block().getId();

        SolicitudModeracionPostVo solicitudModeracionPost = new SolicitudModeracionPostVo();
        solicitudModeracionPost.setPostId(postId);
        solicitudModeracionPost.setEstadoPost(EstadoContenido.APROBADO);

        Mono<Post> aprobacion = moderacionService.cambiarEstado(solicitudModeracionPost);

        StepVerifier
                .create(aprobacion)
                .assertNext(postAprobado -> assertThat(postAprobado.getEstado()).isEqualTo(EstadoContenido.APROBADO))
                .expectComplete()
                .verify();
    }

    @Test
    @WithUserDetails(value = "moderador")
    public void guardar_conEstadoRechazadoYComentarioDeRechazo_seGuardaConElMotivoRechazo() {
        Post postPendiente = new Post();
        postPendiente.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        String postId = postRepository.save(postPendiente).block().getId();

        SolicitudModeracionPostVo solicitudModeracionPost = new SolicitudModeracionPostVo();
        solicitudModeracionPost.setPostId(postId);
        String motivoRechazo = "Motivo rechazo con mas de sesenta caracteres para que sea valido";
        solicitudModeracionPost.setMotivoRechazo(motivoRechazo);
        solicitudModeracionPost.setEstadoPost(EstadoContenido.RECHAZADO);

        Mono<Post> aprobacion = moderacionService.cambiarEstado(solicitudModeracionPost);

        StepVerifier
                .create(aprobacion)
                .assertNext(postAprobado -> {
                    assertThat(postAprobado.getEstado()).isEqualTo(EstadoContenido.RECHAZADO);
                    assertThat(postAprobado.getMotivoRechazo()).isEqualTo(motivoRechazo);
                })
                .expectComplete()
                .verify();
    }

}
