package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.repository.PostRepository;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class BuscadorGeneralPostServiceTest extends ApplicationTests {

    @Autowired
    private BuscadorGeneralPostService busquedaGeneralPostService;

    @Autowired
    private PostRepository postRepository;

    @BeforeEach
    public void cleanDatabase() {
        postRepository.deleteAll().block();
    }

    @Test
    public void buscarPorContenido_postConContenidoBuscado_retornaPostsConContenidoBuscado() {
        Post postConContenidoCorrecto = new Post();
        postConContenidoCorrecto.setEstado(EstadoContenido.APROBADO);
        postConContenidoCorrecto.setContenido("post con contenido correcto");
        String postConContenidoCorrectoId = postRepository.save(postConContenidoCorrecto).block().getId();

        Post otroPostConContenidoCorrecto = new Post();
        otroPostConContenidoCorrecto.setEstado(EstadoContenido.APROBADO);
        otroPostConContenidoCorrecto.setContenido("post con otro contenido correcto");
        String otroPostConContenidoCorrectoId = postRepository.save(otroPostConContenidoCorrecto).block().getId();

        Post postSinContenidoCorrecto = new Post();
        postSinContenidoCorrecto.setContenido("post sin contenido buscado");
        postRepository.save(postSinContenidoCorrecto).block();

        Flux<Post> postsEncontrados = busquedaGeneralPostService.buscarPorContenido("correcto", 0, 3);

        StepVerifier
                .create(postsEncontrados)
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(postConContenidoCorrectoId))
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(otroPostConContenidoCorrectoId))
                .expectComplete()
                .verify();
    }

    @Test
    public void buscarPorContenido_postConContenidoBuscadoPaginado_retornaPostPorPagina() {
        Post postConContenidoCorrecto = new Post();
        postConContenidoCorrecto.setEstado(EstadoContenido.APROBADO);
        postConContenidoCorrecto.setContenido("post con contenido correcto");
        String postConContenidoCorrectoId = postRepository.save(postConContenidoCorrecto).block().getId();

        Post otroPostConContenidoCorrecto = new Post();
        otroPostConContenidoCorrecto.setEstado(EstadoContenido.APROBADO);
        otroPostConContenidoCorrecto.setContenido("post con otro contenido correcto");
        String otroPostConContenidoCorrectoId = postRepository.save(otroPostConContenidoCorrecto).block().getId();

        Post postNoAlcanzadoPorPaginacion = new Post();
        postNoAlcanzadoPorPaginacion.setContenido("post con contenido correcto pero no alcanzado por paginacion");
        postRepository.save(postNoAlcanzadoPorPaginacion).block();

        Flux<Post> postsEncontrados = busquedaGeneralPostService.buscarPorContenido("correcto", 0, 2);

        StepVerifier
                .create(postsEncontrados)
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(postConContenidoCorrectoId))
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(otroPostConContenidoCorrectoId))
                .expectComplete()
                .verify();

    }

    @Test
    public void cantidadPosts_conPostsGuardados_retornaCantidad() {
        Post postConContenidoCorrecto = new Post();
        postConContenidoCorrecto.setEstado(EstadoContenido.APROBADO);
        postConContenidoCorrecto.setContenido("post con contenido correcto");
        postRepository.save(postConContenidoCorrecto).block().getId();

        Post otroPostConContenidoCorrecto = new Post();
        otroPostConContenidoCorrecto.setEstado(EstadoContenido.APROBADO);
        otroPostConContenidoCorrecto.setContenido("post con otro contenido correcto");
        postRepository.save(otroPostConContenidoCorrecto).block().getId();

        Post postSinContenidoCorrecto = new Post();
        postSinContenidoCorrecto.setContenido("post sin contenido buscado");
        postRepository.save(postSinContenidoCorrecto).block();

        Mono<Long> cantidadPosts = busquedaGeneralPostService.cantidadPosts("correcto");

        StepVerifier
                .create(cantidadPosts)
                .assertNext(cantidad -> assertThat(cantidad).isEqualTo(2))
                .expectComplete()
                .verify();
    }

    @Test
    public void buscarPorContenido_postPendienteYOtroAprobado_soloTraeElAprobado() {
        Post unPost = new Post();
        unPost.setEstado(EstadoContenido.APROBADO);
        unPost.setContenido("post con contenido correcto");
        String unPostId = postRepository.save(unPost).block().getId();

        Post otroPost = new Post();
        otroPost.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        otroPost.setContenido("otro post con contenido correcto");
        postRepository.save(otroPost).block().getId();

        Flux<Post> postsEncontrados = busquedaGeneralPostService.buscarPorContenido("correcto", 0, 2);

        StepVerifier
                .create(postsEncontrados)
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(unPostId))
                .expectComplete()
                .verify();
    }

    @Test
    public void cantidadPosts_postPendienteYOtroAprobado_soloTraeElAprobado() {
        Post unPost = new Post();
        unPost.setEstado(EstadoContenido.APROBADO);
        unPost.setContenido("post con contenido correcto");
        postRepository.save(unPost).block().getId();

        Post otroPost = new Post();
        otroPost.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        otroPost.setContenido("otro post con contenido correcto");
        postRepository.save(otroPost).block().getId();

        Mono<Long> cantidadPosts = busquedaGeneralPostService.cantidadPosts("correcto");

        StepVerifier
                .create(cantidadPosts)
                .assertNext(cantidad -> assertThat(cantidad).isEqualTo(1))
                .expectComplete()
                .verify();
    }

}
