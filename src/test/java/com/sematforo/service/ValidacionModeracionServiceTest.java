package com.sematforo.service;

import com.sematforo.ApplicationTests;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidacionModeracionServiceTest extends ApplicationTests {

    @Autowired
    private ValidacionModeracionService validacionModeracionService;

    @Test
    public void validarMotivoRechazo_conEstadoRechazadoYComentarioDeRechazoNull_lanzaExcepcion() {
        assertThatThrownBy(() -> validacionModeracionService.validarMotivoRechazo(null))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("El motivo de rechazo es requerido.");
    }

    @Test
    public void validarMotivoRechazo_conEstadoRechazadoYComentarioDeRechazoConLongitudMenorA60Caracteres_lanzaExcepcion() {
        assertThatThrownBy(() -> validacionModeracionService.validarMotivoRechazo("motivo rechazo menor a 60 caracteres"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Debes escribir el motivo de rechazo con un mínimo de 60 caracteres.");
    }

}
