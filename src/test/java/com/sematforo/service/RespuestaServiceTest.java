package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Respuesta;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.RespuestaRepository;
import com.sematforo.vo.SolicitudRespuestaVo;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class RespuestaServiceTest extends ApplicationTests {

    @Autowired
    private RespuestaRepository respuestaRepository;

    @Autowired
    private RespuestaService respuestaService;

    @BeforeEach
    public void cleanDatabase() {
        respuestaRepository.deleteAll().block();
    }

    @Test
    @WithUserDetails(value = "username")
    public void guardar_respuestaValida_retornaRespuestaGuardada() {
        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        String postId = "un post id";
        String respuesta = "una respuesta";

        SolicitudRespuestaVo solicitudRespuestaVo = new SolicitudRespuestaVo();
        solicitudRespuestaVo.setPostId(postId);
        solicitudRespuestaVo.setRespuesta(respuesta);

        Mono<Respuesta> respuestaGuardada = respuestaService.guardar(solicitudRespuestaVo);

        StepVerifier
                .create(respuestaGuardada)
                .assertNext(respuestaNext -> {
                    assertThat(respuestaNext.getId()).isNotNull();
                    assertThat(respuestaNext.getEstado()).isEqualTo(EstadoContenido.PENDIENTE_DE_APROBACION);
                    assertThat(respuestaNext.getTexto()).isEqualTo(respuesta);
                    assertThat(respuestaNext.getUsuarioId()).isEqualTo(usuarioLogueado.getId());
                    assertThat(respuestaNext.getPostId()).isEqualTo(postId);
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void buscarParaPost_conRespuestasParaUnPostAprobadas_retornaTodasLasRespuestas() {
        String postId = "un postId";

        Respuesta unaRespuesta = new Respuesta();
        unaRespuesta.setPostId(postId);
        unaRespuesta.setEstado(EstadoContenido.APROBADO);
        unaRespuesta.setTexto("Un texto");

        String unaRespuestaId = respuestaRepository.save(unaRespuesta).block().getId();

        Respuesta otraRespuestaPendienteAprobacion = new Respuesta();
        otraRespuestaPendienteAprobacion.setPostId(postId);
        otraRespuestaPendienteAprobacion.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        otraRespuestaPendienteAprobacion.setTexto("Otro texto pendiente aprobacion");

        respuestaRepository.save(otraRespuestaPendienteAprobacion).block();

        Respuesta otraRespuestaRechazada = new Respuesta();
        otraRespuestaRechazada.setPostId(postId);
        otraRespuestaRechazada.setEstado(EstadoContenido.RECHAZADO);
        otraRespuestaRechazada.setTexto("Otro texto rechazada");

        respuestaRepository.save(otraRespuestaRechazada).block();

        Respuesta otraRespuesta = new Respuesta();
        otraRespuesta.setPostId("otro post id");
        otraRespuesta.setTexto("otro texto");

        respuestaRepository.save(otraRespuesta).block();

        Flux<Respuesta> respuestasParaPostId = respuestaService.buscarParaPost(postId);

        StepVerifier
                .create(respuestasParaPostId)
                .assertNext(respuestaNext -> assertThat(respuestaNext.getId()).isEqualTo(unaRespuestaId))
                .expectComplete()
                .verify();

    }

}
