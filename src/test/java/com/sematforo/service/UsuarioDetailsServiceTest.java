package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.UsuarioRepository;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class UsuarioDetailsServiceTest extends ApplicationTests {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    @BeforeEach
    public void cleanDatabase() {
        usuarioRepository.deleteAll().block();
    }

    @Test
    public void buscarPorUsername_ususarioExiste_retornUsuario() {

        String username = "username";

        Usuario usuario = new Usuario();
        usuario.setPassword("password");
        usuario.setUsername(username);

        usuarioRepository.save(usuario).block();

        Mono<UserDetails> resultado = usuarioService.findByUsername(username);

        StepVerifier
                .create(resultado)
                .assertNext(usuarioEncontrado -> {
                    assertEquals(usuarioEncontrado.getPassword(), usuario.getPassword());
                    assertEquals(usuarioEncontrado.getUsername(), usuario.getUsername());
                })
                .expectComplete()
                .verify();

    }

    @Test
    public void verificarExistenciaPorUsername_usuarioConUsername_retornaTrue() {
        String username = "unUsername";

        Usuario usuario = new Usuario();
        usuario.setUsername(username);

        usuarioRepository.save(usuario).block();

        Mono<Boolean> existencia = usuarioService.verificarExistenciaPorUsername(username);

        StepVerifier
                .create(existencia)
                .assertNext(usuarioEncontrado -> assertTrue(usuarioEncontrado))
                .expectComplete()
                .verify();
    }

    @Test
    public void verificarExistenciaPorUsername_usuarioConUsername_retornaFalse() {
        String username = "unUsername";
        Mono<Boolean> existencia = usuarioService.verificarExistenciaPorUsername(username);

        StepVerifier
                .create(existencia)
                .assertNext(usuarioEncontrado -> assertFalse(usuarioEncontrado))
                .expectComplete()
                .verify();
    }

    @Test
    public void confirmarCuenta_conNombreUsuarioExistenteConEnabledEnFalseYIdCorrecto_seSeteaEnableEnTrue() {
        Usuario usuario = new Usuario();
        usuario.setEnabled(false);
        String nombreUsuario = "fhipper";
        usuario.setUsername(nombreUsuario);

        usuarioRepository.save(usuario).block();

        String usuarioId = usuario.getId();

        Mono<Usuario> cuentaConfirmada = usuarioService.cuentaConfirmada(nombreUsuario, usuarioId);
        StepVerifier
                .create(cuentaConfirmada)
                .assertNext(cuenta -> assertTrue(cuenta.isEnabled()))
                .expectComplete()
                .verify();

    }

    @Test
    public void confirmarCuenta_conNombreUsuarioNoExisteConEnabledEnFalseYIdCorrecto_noDevuelveUsuario() {
        String idUsuario = "99999";

        Mono<Usuario> cuentaConfirmada = usuarioService.cuentaConfirmada("usernameNoExiste", idUsuario);

        StepVerifier
                .create(cuentaConfirmada)
                .expectComplete()
                .verify();
    }

    @Test
    public void confirmarCuenta_conNombreUsuarioExistenteConEnabledEnFalseYHashIncorrecto_sigueEnableEnFalse() {
        Usuario usuario = new Usuario();
        usuario.setEnabled(false);
        String nombreUsuario = "fhipper";
        usuario.setUsername(nombreUsuario);

        usuarioRepository.save(usuario).block();

        String usuarioIdIncorrecto = "c81e728d";

        Mono<Usuario> cuentaConfirmada = usuarioService.cuentaConfirmada(nombreUsuario, usuarioIdIncorrecto);

        StepVerifier
                .create(cuentaConfirmada)
                .expectComplete()
                .verify();
    }
}
