package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.Post;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.PostRepository;
import java.time.LocalDateTime;
import java.time.Month;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class BusquedaMisPostsServiceTest extends ApplicationTests {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private BusquedaMisPostsService busquedaMisPostsService;

    @BeforeEach
    public void cleanDatabase() {
        postRepository.deleteAll().block();
    }

    @Test
    @WithUserDetails(value = "username")
    public void buscarTodos_conUsuario_retornanPostDeUsuarioLogueado() {
        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        String contenidoOtroPost = "contenido libre para otro post.";

        Post unPostDeUsuarioLogueado = new Post();
        unPostDeUsuarioLogueado.setContenido(contenidoOtroPost);
        unPostDeUsuarioLogueado.setUsuarioId(usuarioLogueado.getId());
        unPostDeUsuarioLogueado.setFechaHora(LocalDateTime.of(2020, Month.APRIL, 1, 22, 00));

        String unPostDeUsuarioLogueadoId = postRepository.save(unPostDeUsuarioLogueado).block().getId();

        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post otroPostNoObtenidoPorSerDeOtroUsuario = new Post();
        otroPostNoObtenidoPorSerDeOtroUsuario.setContenido(contenido);
        otroPostNoObtenidoPorSerDeOtroUsuario.setUsuarioId("otroUsuario");
        otroPostNoObtenidoPorSerDeOtroUsuario.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        postRepository.save(otroPostNoObtenidoPorSerDeOtroUsuario).block().getId();

        String contenidoPostNoObtenido = "contenido libre para otro post.";

        Post oroPostNoObtenidoPorPaginacion = new Post();
        oroPostNoObtenidoPorPaginacion.setContenido(contenidoPostNoObtenido);
        oroPostNoObtenidoPorPaginacion.setUsuarioId(usuarioLogueado.getId());
        oroPostNoObtenidoPorPaginacion.setFechaHora(LocalDateTime.of(2019, Month.APRIL, 1, 22, 00));

        postRepository.save(oroPostNoObtenidoPorPaginacion).block();

        Pageable pageable = PageRequest.of(0, 1);

        Flux<Post> postsObtenidos = busquedaMisPostsService.buscarTodos(pageable);

        StepVerifier
                .create(postsObtenidos)
                .assertNext(post -> {
                    assertEquals(post.getId(), unPostDeUsuarioLogueadoId);
                    assertEquals(post.getContenido(), unPostDeUsuarioLogueado.getContenido());
                    assertEquals(post.getFechaHora(), unPostDeUsuarioLogueado.getFechaHora());
                })
                .expectComplete()
                .verify();

    }
    
    @Test
    @WithUserDetails(value = "username")
    public void buscarTodos_conOrdenPorFecha_retornanPostDeUsuarioLogueado() {
        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        Post unPost = new Post();
        unPost.setUsuarioId(usuarioLogueado.getId());
        unPost.setFechaHora(LocalDateTime.of(2019, Month.APRIL, 1, 22, 00));

        String unPostId = postRepository.save(unPost).block().getId();

        Post otroPost = new Post();
        otroPost.setUsuarioId(usuarioLogueado.getId());
        otroPost.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String otroPostId = postRepository.save(otroPost).block().getId();

        Pageable pageable = PageRequest.of(0, 2);

        Flux<Post> postsObtenidos = busquedaMisPostsService.buscarTodos(pageable);

        StepVerifier
                .create(postsObtenidos)
                .assertNext(post -> assertEquals(post.getId(), otroPostId))
                .assertNext(post -> assertEquals(post.getId(), unPostId))
                .expectComplete()
                .verify();

    }

    @Test
    @WithUserDetails(value = "username")
    public void cantidadMisPosts_conPostsExisten_retornaCantidadPosts() {
        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post post = new Post();
        post.setContenido(contenido);
        post.setUsuarioId(usuarioLogueado.getId());
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        postRepository.save(post).block();

        String contenidoOtroPost = "contenido libre para otro post.";

        Post otroPost = new Post();
        otroPost.setContenido(contenidoOtroPost);
        otroPost.setUsuarioId(usuarioLogueado.getId());
        otroPost.setFechaHora(LocalDateTime.of(2020, Month.APRIL, 1, 22, 00));

        postRepository.save(otroPost).block();
        
        String contenidoOtroUsuarioPost = "contenido libre para otro post.";

        Post otroUsuarioPost = new Post();
        otroUsuarioPost.setContenido(contenidoOtroUsuarioPost);
        otroUsuarioPost.setUsuarioId("otroUsuario");
        otroUsuarioPost.setFechaHora(LocalDateTime.of(2020, Month.APRIL, 1, 22, 00));

        postRepository.save(otroUsuarioPost).block();

        Mono<Long> cantidadPosts = busquedaMisPostsService.cantidadMisPosts();

        StepVerifier
                .create(cantidadPosts)
                .assertNext(cantidad -> assertEquals(cantidad, 2))
                .expectComplete()
                .verify();

    }

}
