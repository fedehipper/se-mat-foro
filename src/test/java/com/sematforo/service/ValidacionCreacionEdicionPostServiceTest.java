package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.vo.SolicitudPostVo;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidacionCreacionEdicionPostServiceTest extends ApplicationTests {

    @Autowired
    private ValidacionCreacionEdicionPostService validacionCreacionEdicionPostService;

    @Test
    public void validacionCreacionEdicionPostService_postConContenidoCortoMenosDe30Caracteres_lanzaExcepcion() {
        String titulo = "Titulo";
        String contenidoMenorAlMinimo = "Consulta no supera el minimo";

        SolicitudPostVo postVo = new SolicitudPostVo();
        postVo.setTitulo(titulo);
        postVo.setContenido(contenidoMenorAlMinimo);

        assertThatThrownBy(() -> validacionCreacionEdicionPostService.validar(postVo))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Debes escribir tu consulta con un mínimo de 30 caracteres.");
    }

    @Test
    public void validacionCreacionEdicionPostService_postConTituloVacio_lanzaExcepcion() {
        String titulo = "";
        String contenido = "un Contenido valido mas de treinta caracteres";

        SolicitudPostVo postVo = new SolicitudPostVo();
        postVo.setTitulo(titulo);
        postVo.setContenido(contenido);

        assertThatThrownBy(() -> validacionCreacionEdicionPostService.validar(postVo))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("El título es obligatorio.");
    }

    @Test
    public void validacionCreacionEdicionPostService_postConTituloNull_lanzaExcepcion() {
        String titulo = null;
        String contenido = "un Contenido valido mas de treinta caracteres";

        SolicitudPostVo postVo = new SolicitudPostVo();
        postVo.setTitulo(titulo);
        postVo.setContenido(contenido);

        assertThatThrownBy(() -> validacionCreacionEdicionPostService.validar(postVo))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("El título es obligatorio.");
    }

    @Test
    public void validacionCreacionEdicionPostService_postConEtiquetasVacias_lanzaExcepcion() {
        String titulo = "un titulo";
        String contenido = "un Contenido valido mas de treinta caracteres";
        List<String> etiquetas = List.of();

        SolicitudPostVo postVo = new SolicitudPostVo();
        postVo.setTitulo(titulo);
        postVo.setEtiquetas(etiquetas);
        postVo.setContenido(contenido);

        assertThatThrownBy(() -> validacionCreacionEdicionPostService.validar(postVo))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Debes incluir al menos una etiqueta que describa tu post.");
    }

}
