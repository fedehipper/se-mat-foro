package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.Notificacion;
import com.sematforo.domain.TipoContenido;
import com.sematforo.domain.TipoNotificacion;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.NotificacionRepository;
import com.sematforo.vo.NotificacionVo;
import com.sematforo.vo.SolicitudNotificacionVo;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class NotificacionServiceTest extends ApplicationTests {

    @Autowired
    private NotificacionService notificacionService;

    @Autowired
    private NotificacionRepository notificacionRepository;

    @BeforeEach
    public void cleanDatabase() {
        notificacionRepository.deleteAll().block();
    }

    @Test
    public void guardar_aprobacionPregunta_seGuardaLaNotificacion() {
        String usuarioId = "usuarioId";
        String postId = "postId";
        String info = "Info del post";

        SolicitudNotificacionVo solicitudNotificacionVo = new SolicitudNotificacionVo();
        solicitudNotificacionVo.setContenidoId(postId);
        solicitudNotificacionVo.setInfo(info);
        solicitudNotificacionVo.setUsuarioId(usuarioId);
        solicitudNotificacionVo.setTipo(TipoNotificacion.PREGUNTA_APROBADA);

        Mono<Notificacion> notificacion = notificacionService.guardar(solicitudNotificacionVo);

        StepVerifier
                .create(notificacion)
                .assertNext(notificacionCreada -> {
                    assertThat(notificacionCreada.getId()).isNotNull();
                    assertThat(notificacionCreada.getInfo()).isEqualTo(info);
                    assertThat(notificacionCreada.getTipo()).isEqualTo(TipoNotificacion.PREGUNTA_APROBADA);
                    assertThat(notificacionCreada.getUsuarioId()).isEqualTo(usuarioId);
                    assertThat(notificacionCreada.getContenidoId()).isEqualTo(postId);
                })
                .expectComplete()
                .verify();

    }

    @Test
    public void guardar_rechazoPregunta_seGuardaLaNotificacion() {
        String usuarioId = "usuarioId";
        String postId = "postId";

        SolicitudNotificacionVo solicitudNotificacionVo = new SolicitudNotificacionVo();
        solicitudNotificacionVo.setContenidoId(postId);
        solicitudNotificacionVo.setUsuarioId(usuarioId);
        solicitudNotificacionVo.setTipo(TipoNotificacion.PREGUNTA_RECHAZADA);

        Mono<Notificacion> notificacion = notificacionService.guardar(solicitudNotificacionVo);

        StepVerifier
                .create(notificacion)
                .assertNext(notificacionCreada -> {
                    assertThat(notificacionCreada.getId()).isNotNull();
                    assertThat(notificacionCreada.getTipo()).isEqualTo(TipoNotificacion.PREGUNTA_RECHAZADA);
                    assertThat(notificacionCreada.getUsuarioId()).isEqualTo(usuarioId);
                    assertThat(notificacionCreada.getContenidoId()).isEqualTo(postId);
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void guardar_preguntaEnRevision_seGuardaLaNotificacion() {
        String usuarioId = "usuarioId";
        String postId = "postId";

        SolicitudNotificacionVo solicitudNotificacionVo = new SolicitudNotificacionVo();
        solicitudNotificacionVo.setContenidoId(postId);
        solicitudNotificacionVo.setTipoContenido(TipoContenido.PREGUNTA);
        solicitudNotificacionVo.setUsuarioId(usuarioId);
        solicitudNotificacionVo.setTipo(TipoNotificacion.PREGUNTA_EN_REVISION);

        Mono<Notificacion> notificacion = notificacionService.guardar(solicitudNotificacionVo);

        StepVerifier
                .create(notificacion)
                .assertNext(notificacionCreada -> {
                    assertThat(notificacionCreada.getId()).isNotNull();
                    assertThat(notificacionCreada.getTipoContenido()).isEqualTo(TipoContenido.PREGUNTA);
                    assertThat(notificacionCreada.getTipo()).isEqualTo(TipoNotificacion.PREGUNTA_EN_REVISION);
                    assertThat(notificacionCreada.getUsuarioId()).isEqualTo(usuarioId);
                    assertThat(notificacionCreada.getContenidoId()).isEqualTo(postId);
                })
                .expectComplete()
                .verify();
    }

    @Test
    @WithUserDetails(value = "username")
    public void buscarTodas_conNotificacionesParaUsuarioLogueado_retornaNotificaciones() {

        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        Notificacion notificacion = new Notificacion();
        notificacion.setUsuarioId(usuarioLogueado.getId());
        String info = "info";
        notificacion.setInfo(info);
        notificacion.setContenidoId("un post id");
        notificacion.setTipo(TipoNotificacion.PREGUNTA_APROBADA);

        String notificacionId = notificacionRepository.save(notificacion).block().getId();

        Notificacion notificacionOtroUsuario = new Notificacion();
        notificacionOtroUsuario.setUsuarioId("otro usuario id");
        notificacionOtroUsuario.setContenidoId("un post id");
        notificacionOtroUsuario.setTipo(TipoNotificacion.PREGUNTA_APROBADA);

        notificacionRepository.save(notificacionOtroUsuario).block();

        Flux<NotificacionVo> notificaciones = notificacionService.buscarTodas();

        StepVerifier
                .create(notificaciones)
                .assertNext(notificacionEncontrada -> {
                    assertThat(notificacionEncontrada.getId()).isEqualTo(notificacionId);
                    assertThat(notificacionEncontrada.getInfo()).isEqualTo(info);
                    assertThat(notificacionEncontrada.getUsuarioId()).isEqualTo(usuarioLogueado.getId());
                    assertThat(notificacionEncontrada.getMensaje()).isEqualTo(notificacion.getTipo().getMensaje());
                })
                .expectComplete()
                .verify();

    }

    @Test
    @WithUserDetails(value = "username")
    public void eliminarNotificacion_conNotificacionCreadaDeUsuarioLogueado_seEliminaLaNotificacion() {
        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        Notificacion notificacion = new Notificacion();
        notificacion.setUsuarioId(usuarioLogueado.getId());
        String info = "info";
        notificacion.setInfo(info);
        notificacion.setContenidoId("un post id");
        notificacion.setTipo(TipoNotificacion.PREGUNTA_APROBADA);

        String notificacionId = notificacionRepository.save(notificacion).block().getId();

        Mono<Void> eliminacionNotificacion = notificacionService.eliminar(notificacionId);

        StepVerifier
                .create(eliminacionNotificacion)
                .expectComplete()
                .verify();

    }

    @Test
    @WithUserDetails(value = "username")
    public void buscarTodas_conNotificacionesParaUsuarioLogueado_retornaNotificacionesEnOrdenDecreciente() {
        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        Notificacion unaNotificacion = new Notificacion();
        unaNotificacion.setContenidoId("un contenido");
        unaNotificacion.setInfo("una info");
        unaNotificacion.setTipo(TipoNotificacion.PREGUNTA_APROBADA);
        unaNotificacion.setTipoContenido(TipoContenido.PREGUNTA);
        unaNotificacion.setUsuarioId(usuarioLogueado.getId());
        String unaNotificacionId = notificacionRepository.save(unaNotificacion).block().getId();

        Notificacion otraNotificacion = new Notificacion();
        otraNotificacion.setContenidoId("un contenido");
        otraNotificacion.setInfo("una info");
        otraNotificacion.setTipo(TipoNotificacion.PREGUNTA_APROBADA);
        otraNotificacion.setTipoContenido(TipoContenido.PREGUNTA);
        otraNotificacion.setUsuarioId(usuarioLogueado.getId());
        String otraNotificacionId = notificacionRepository.save(otraNotificacion).block().getId();

        Flux<NotificacionVo> notificaciones = notificacionService.buscarTodas();

        StepVerifier
                .create(notificaciones)
                .assertNext(segundaNotificacion -> assertThat(segundaNotificacion.getId()).isEqualTo(otraNotificacionId))
                .assertNext(primerNotificacion -> assertThat(primerNotificacion.getId()).isEqualTo(unaNotificacionId))
                .expectComplete()
                .verify();
    }

}
