package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Respuesta;
import com.sematforo.repository.RespuestaRepository;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class ModeracionRespuestaServiceTest extends ApplicationTests {

    @Autowired
    private ModeracionRespuestaService moderacionRespuestaService;

    @Autowired
    private RespuestaRepository respuestaRepository;

    @BeforeEach
    public void cleanDatabase() {
        respuestaRepository.deleteAll().block();
    }

    @Test
    public void buscarTodas_estadoPendienteAprobacion_retornaRespuestasPendientesAprobacion() {
        Respuesta respuesta = new Respuesta();
        respuesta.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        respuesta.setPostId("un post id");
        respuesta.setTexto("un texto");

        respuestaRepository.save(respuesta).block();

        Flux<Respuesta> respuestas = moderacionRespuestaService.buscarTodas();

        StepVerifier
                .create(respuestas)
                .assertNext(respuestaEncontrada -> assertThat(respuestaEncontrada.getId()).isEqualTo(respuesta.getId()))
                .expectComplete()
                .verify();

    }

    @Test
    public void cambiarEstado_respuestaConEstadoPendienteAprobacion_seGuardaRespuestaConEstadoAprobado() {
        Respuesta respuesta = new Respuesta();
        respuesta.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);

        String respuestaId = respuestaRepository.save(respuesta).block().getId();

        Mono<Respuesta> respuestaAprobada = moderacionRespuestaService.cambiarEstado(respuestaId, EstadoContenido.APROBADO);

        StepVerifier
                .create(respuestaAprobada)
                .assertNext(respuestaNext -> {
                    assertThat(respuestaNext.getEstado()).isEqualTo(EstadoContenido.APROBADO);
                    assertThat(respuestaNext.getId()).isEqualTo(respuestaId);
                })
                .expectComplete()
                .verify();

    }

    @Test
    public void cambiarEstado_respuestaConEstadoPendienteAprobacion_seGuardaRespuestaConEstadoRechazado() {
        Respuesta respuesta = new Respuesta();
        respuesta.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);

        String respuestaId = respuestaRepository.save(respuesta).block().getId();

        Mono<Respuesta> respuestaAprobada = moderacionRespuestaService.cambiarEstado(respuestaId, EstadoContenido.RECHAZADO);

        StepVerifier
                .create(respuestaAprobada)
                .assertNext(respuestaNext -> {
                    assertThat(respuestaNext.getEstado()).isEqualTo(EstadoContenido.RECHAZADO);
                    assertThat(respuestaNext.getId()).isEqualTo(respuestaId);
                })
                .expectComplete()
                .verify();
    }

}
