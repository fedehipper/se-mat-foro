package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.PostRepository;
import com.sematforo.vo.SolicitudPostVo;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class EdicionPostServiceTest extends ApplicationTests {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private EdicionPostService edicionPostService;

    @BeforeEach
    public void cleanDatabase() {
        postRepository.deleteAll().block();
    }

    @Test
    @WithUserDetails(value = "username")
    public void editarPost_postExisten_retornaPostEditado() {
        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        String usuarioLogueadoId = ((Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal()).getId();

        Post post = new Post();
        post.setContenido(contenido);
        post.setUsuarioId(usuarioLogueadoId);
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));
        post.setEtiquetas(List.of("unaEtiqueta", "otraEtiqueta"));

        String postGuardadoId = postRepository.save(post).block().getId();

        SolicitudPostVo postVo = new SolicitudPostVo();
        postVo.setContenido(" contenido editado con largo de caracteres valido mayor a 30 ");
        postVo.setTitulo(" titulo editado ");
        postVo.setEtiquetas(List.of("unaEtiqueta", "otraEtiquetaModificada"));

        Mono<Post> postEditado = edicionPostService.editarPost(postGuardadoId, postVo);

        StepVerifier
                .create(postEditado)
                .assertNext(postObtenido -> {
                    assertEquals(postObtenido.getContenido(), postVo.getContenido().trim());
                    assertThat(postObtenido.getFechaHora().isAfter(post.getFechaHora()));
                    assertEquals(postObtenido.getEtiquetas(), postVo.getEtiquetas());
                    assertEquals(postObtenido.getTitulo(), postVo.getTitulo().trim());
                    assertThat(postObtenido.getMotivoRechazo()).isNull();
                    assertEquals(postObtenido.getEstado(), EstadoContenido.PENDIENTE_DE_APROBACION);
                })
                .expectComplete()
                .verify();
    }

    @Test
    @WithUserDetails(value = "username")
    public void editarPost_postConMotivoRechazo_retornaPostEditadoConMotivoRechazoNull() {
        String usuarioLogueadoId = ((Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal()).getId();

        Post post = new Post();
        post.setUsuarioId(usuarioLogueadoId);
        post.setMotivoRechazo("Motivo rechazo");

        String postGuardadoId = postRepository.save(post).block().getId();

        SolicitudPostVo postVo = new SolicitudPostVo();
        postVo.setContenido(" contenido editado con largo de caracteres valido mayor a 30 ");
        postVo.setTitulo(" titulo editado ");
        postVo.setEtiquetas(List.of("unaEtiqueta", "otraEtiquetaModificada"));

        Mono<Post> postEditado = edicionPostService.editarPost(postGuardadoId, postVo);

        StepVerifier
                .create(postEditado)
                .assertNext(postObtenido -> assertThat(postObtenido.getMotivoRechazo()).isNull())
                .expectComplete()
                .verify();
    }

    @Test
    @WithUserDetails(value = "username")
    public void editarPost_usuarioLogueadoEditaPostDeOtroUsuario_lanzaExcepcion() {
        Post post = new Post();
        post.setUsuarioId("unIdDiferenteAUsuarioLogueadoId");
        String postId = postRepository.save(post).block().getId();

        SolicitudPostVo solicitudPostVo = new SolicitudPostVo();
        solicitudPostVo.setContenido(" contenido editado con largo de caracteres valido mayor a 30 ");
        solicitudPostVo.setTitulo(" titulo editado ");
        solicitudPostVo.setEtiquetas(List.of("unaEtiqueta", "otraEtiquetaModificada"));

        Mono<Post> postEditado = edicionPostService.editarPost(postId, solicitudPostVo);

        StepVerifier
                .create(postEditado)
                .expectError(AccessDeniedException.class)
                .verify();
    }
}
