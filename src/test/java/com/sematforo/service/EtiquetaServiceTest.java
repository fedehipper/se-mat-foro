package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.Etiqueta;
import com.sematforo.repository.EtiquetaRepository;
import com.sematforo.vo.SolicitudEtiquetaVo;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

public class EtiquetaServiceTest extends ApplicationTests {

    @Autowired
    private EtiquetaRepository etiquetaRepository;

    @Autowired
    private EtiquetaService etiquetaService;

    @BeforeEach
    public void cleanDatabase() {
        etiquetaRepository.deleteAll().block();
    }

    @Test
    public void guardar_conEtiqueaNoExistenYPostIdCreado_retornaEtiquetaGuardada() {
        String postId = "un post id";
        SolicitudEtiquetaVo solicitudUnaEtiqueta = new SolicitudEtiquetaVo();
        solicitudUnaEtiqueta.setEtiqueta("unaEtiqueta");
        solicitudUnaEtiqueta.setPostId(postId);

        SolicitudEtiquetaVo solicitudOtraEtiqueta = new SolicitudEtiquetaVo();
        solicitudOtraEtiqueta.setEtiqueta("otraEtiqueta");
        solicitudOtraEtiqueta.setPostId(postId);

        Flux<SolicitudEtiquetaVo> solicitudEtiquetas = Flux.just(solicitudUnaEtiqueta, solicitudOtraEtiqueta);

        Flux<Etiqueta> etiquetasGuardadas = etiquetaService.guardar(solicitudEtiquetas);

        StepVerifier
                .create(etiquetasGuardadas)
                .assertNext(etiqueta -> {
                    assertThat(etiqueta.getId()).isNotNull();
                    assertThat(etiqueta.getNombre()).isEqualTo("unaEtiqueta");
                    assertThat(etiqueta.getPostId()).isEqualTo(postId);
                })
                .assertNext(etiqueta -> {
                    assertThat(etiqueta.getId()).isNotNull();
                    assertThat(etiqueta.getNombre()).isEqualTo("otraEtiqueta");
                    assertThat(etiqueta.getPostId()).isEqualTo(postId);
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void actualizar_conEtiquetasNuevasYConPostYaActualizado_seEliminanLasAnterioresYSeGuardanLasNuevas() {
        String postId = "un post id";

        Etiqueta unaEtiquetaGuardada = new Etiqueta();
        unaEtiquetaGuardada.setNombre("unaEtiqueta");
        unaEtiquetaGuardada.setPostId(postId);
        etiquetaRepository.save(unaEtiquetaGuardada).block();

        Etiqueta otraEtiquetaGuardada = new Etiqueta();
        otraEtiquetaGuardada.setNombre("otraEtiqueta");
        otraEtiquetaGuardada.setPostId(postId);
        etiquetaRepository.save(otraEtiquetaGuardada).block();

        etiquetaService.eliminarTodas(postId).block();

        StepVerifier
                .create(etiquetaRepository.count())
                .assertNext(cantidadEtiquetas -> {
                    assertThat(cantidadEtiquetas).isEqualTo(0L);
                })
                .expectComplete()
                .verify();
    }

}
