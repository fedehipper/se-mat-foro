package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Etiqueta;
import com.sematforo.domain.Post;
import com.sematforo.repository.EtiquetaRepository;
import com.sematforo.repository.PostRepository;
import java.time.LocalDateTime;
import java.time.Month;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class BusquedaPostServiceTest extends ApplicationTests {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private BusquedaPostService busquedaPostService;

    @Autowired
    private EtiquetaRepository etiquetaRepository;

    @BeforeEach
    public void cleanDatabase() {
        etiquetaRepository.deleteAll().block();
        postRepository.deleteAll().block();
    }

    @Test
    public void buscarTodos_paginadoPrimeraPaginaConDosResultadoOrdenadosPorMasReciente_retornaTodos() {
        String contenidoOtroPost = "contenido libre para otro post.";

        Post postMasReciente = new Post();
        postMasReciente.setEstado(EstadoContenido.APROBADO);
        postMasReciente.setContenido(contenidoOtroPost);
        postMasReciente.setFechaHora(LocalDateTime.of(2020, Month.APRIL, 1, 22, 00));

        String otroPostId = postRepository.save(postMasReciente).block().getId();

        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post segundoPostMasReciente = new Post();
        segundoPostMasReciente.setEstado(EstadoContenido.APROBADO);
        segundoPostMasReciente.setContenido(contenido);
        segundoPostMasReciente.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String postId = postRepository.save(segundoPostMasReciente).block().getId();

        String contenidoPostNoObtenido = "contenido libre para otro post.";

        Post postNoObtenidoPorPaginacion = new Post();
        postNoObtenidoPorPaginacion.setEstado(EstadoContenido.APROBADO);
        postNoObtenidoPorPaginacion.setContenido(contenidoPostNoObtenido);
        postNoObtenidoPorPaginacion.setFechaHora(LocalDateTime.of(2019, Month.APRIL, 1, 22, 00));

        postRepository.save(postNoObtenidoPorPaginacion).block();

        Pageable pageable = PageRequest.of(0, 2);

        Flux<Post> postsObtenidos = busquedaPostService.buscarTodos(pageable);

        StepVerifier
                .create(postsObtenidos)
                .assertNext(primerPostObtenido -> {
                    assertEquals(primerPostObtenido.getId(), otroPostId);
                    assertEquals(primerPostObtenido.getContenido(), postMasReciente.getContenido());
                    assertEquals(primerPostObtenido.getFechaHora(), postMasReciente.getFechaHora());
                })
                .assertNext(segundoPostObtenido -> {
                    assertEquals(segundoPostObtenido.getId(), postId);
                    assertEquals(segundoPostObtenido.getContenido(), segundoPostMasReciente.getContenido());
                    assertEquals(segundoPostObtenido.getFechaHora(), segundoPostMasReciente.getFechaHora());
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void buscarPorId_postExistente_retornaPost() {
        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post post = new Post();
        post.setContenido(contenido);
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String postGuardadoId = postRepository.save(post).block().getId();

        Mono<Post> postObtenido = busquedaPostService.buscarPorId(postGuardadoId);

        StepVerifier
                .create(postObtenido)
                .assertNext(postBuscado -> {
                    assertEquals(postBuscado.getContenido(), post.getContenido());
                    assertEquals(postBuscado.getId(), postGuardadoId);
                    assertEquals(postBuscado.getFechaHora(), post.getFechaHora());
                })
                .expectComplete()
                .verify();
    }

    @Test
    public void cantidadPosts_conPostGuardados_retornaLaCantidad() {
        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post post = new Post();
        post.setEstado(EstadoContenido.APROBADO);
        post.setContenido(contenido);
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        postRepository.save(post).block();

        String contenidoOtroPost = "contenido libre para otro post.";

        Post otroPost = new Post();
        otroPost.setEstado(EstadoContenido.APROBADO);
        otroPost.setContenido(contenidoOtroPost);
        otroPost.setFechaHora(LocalDateTime.of(2020, Month.APRIL, 1, 22, 00));

        postRepository.save(otroPost).block();

        Mono<Long> cantidadPosts = busquedaPostService.cantidadPosts();

        StepVerifier
                .create(cantidadPosts)
                .assertNext(cantidad -> assertEquals(cantidad, 2))
                .expectComplete()
                .verify();

    }

    @Test
    public void cantidadPostsPorEtiqueta_conEtiquetasCreadas_retornaCantidadPorEtiquetaBuscada() {
        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post post = new Post();
        post.setEstado(EstadoContenido.APROBADO);
        post.setContenido(contenido);
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String postId = postRepository.save(post).block().getId();

        Etiqueta etiqueta = new Etiqueta();
        String nombreEtiqueta = "unaEtiqueta";
        etiqueta.setNombre(nombreEtiqueta);
        etiqueta.setPostId(postId);

        etiquetaRepository.save(etiqueta).block();

        Mono<Long> cantidadPostsPorEtiqueta = busquedaPostService.cantidadPostsPorEtiqueta(nombreEtiqueta);

        StepVerifier
                .create(cantidadPostsPorEtiqueta)
                .assertNext(cantidadPosts -> {
                    assertThat(cantidadPosts).isEqualTo(1L);
                })
                .expectComplete()
                .verify();
    }
    
    @Test
    public void cantidadPostsPorEtiqueta_conPostAprobadosYPendientes_retornaCantidadDeAprobados() {
        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post post = new Post();
        post.setContenido(contenido);
        post.setEstado(EstadoContenido.APROBADO);
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String postId = postRepository.save(post).block().getId();
        
        Post otroPost = new Post();
        otroPost.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        otroPost.setContenido(contenido);
        otroPost.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String otroPostId = postRepository.save(otroPost).block().getId();

        Etiqueta etiqueta = new Etiqueta();
        String nombreEtiqueta = "unaEtiqueta";
        etiqueta.setNombre(nombreEtiqueta);
        etiqueta.setPostId(postId);

        etiquetaRepository.save(etiqueta).block();

        Etiqueta otraEtiqueta = new Etiqueta();
        otraEtiqueta.setNombre(nombreEtiqueta);
        otraEtiqueta.setPostId(otroPostId);

        etiquetaRepository.save(otraEtiqueta).block();

        Mono<Long> cantidadPostsPorEtiqueta = busquedaPostService.cantidadPostsPorEtiqueta(nombreEtiqueta);

        StepVerifier
                .create(cantidadPostsPorEtiqueta)
                .assertNext(cantidadPosts -> assertThat(cantidadPosts).isEqualTo(1L))
                .expectComplete()
                .verify();
    }

    @Test
    public void buscarPorEtiqueta_conEtiquetasExisten_retornaPostsPorEtiqueta() {
        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post post = new Post();
        post.setEstado(EstadoContenido.APROBADO);
        post.setContenido(contenido);
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String postId = postRepository.save(post).block().getId();

        String otroContenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post otroPost = new Post();
        otroPost.setEstado(EstadoContenido.APROBADO);
        otroPost.setContenido(otroContenido);
        otroPost.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String otroPostId = postRepository.save(otroPost).block().getId();

        Etiqueta etiqueta = new Etiqueta();
        String nombreEtiqueta = "unaEtiqueta";
        etiqueta.setNombre(nombreEtiqueta);
        etiqueta.setPostId(postId);

        etiquetaRepository.save(etiqueta).block();

        Etiqueta otraEtiqueta = new Etiqueta();
        otraEtiqueta.setNombre(nombreEtiqueta);
        otraEtiqueta.setPostId(otroPostId);

        etiquetaRepository.save(otraEtiqueta).block();

        Flux<Post> postsPorEtiqueta = busquedaPostService.buscarPorEtiqueta(nombreEtiqueta, PageRequest.of(0, 2));

        StepVerifier
                .create(postsPorEtiqueta)
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(postId))
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(otroPostId))
                .expectComplete()
                .verify();
    }

    @Test
    public void buscarPorEtiqueta_conPostsSinAprobarYAprobados_retornaPostsPorEtiquetaAprobados() {
        String contenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post post = new Post();
        post.setContenido(contenido);
        post.setEstado(EstadoContenido.APROBADO);
        post.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String postId = postRepository.save(post).block().getId();

        String otroContenido = "$b (x - h) = B\\left( x - \\frac{-C}{B} \\right)$";

        Post otroPost = new Post();
        otroPost.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        otroPost.setContenido(otroContenido);
        otroPost.setFechaHora(LocalDateTime.of(2020, Month.MARCH, 1, 22, 00));

        String otroPostId = postRepository.save(otroPost).block().getId();

        Etiqueta etiqueta = new Etiqueta();
        String nombreEtiqueta = "unaEtiqueta";
        etiqueta.setNombre(nombreEtiqueta);
        etiqueta.setPostId(postId);

        etiquetaRepository.save(etiqueta).block();

        Etiqueta otraEtiqueta = new Etiqueta();
        otraEtiqueta.setNombre(nombreEtiqueta);
        otraEtiqueta.setPostId(otroPostId);

        etiquetaRepository.save(otraEtiqueta).block();

        Flux<Post> postsPorEtiqueta = busquedaPostService.buscarPorEtiqueta(nombreEtiqueta, PageRequest.of(0, 2));

        StepVerifier
                .create(postsPorEtiqueta)
                .assertNext(postEncontrado -> assertThat(postEncontrado.getId()).isEqualTo(postId))
                .expectComplete()
                .verify();
    }
    
    @Test
    public void buscarTodos_postPendientesYAprobados_retornaAprobados() {

        Post unPost = new Post();
        unPost.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        postRepository.save(unPost).block().getId();

        Post segundoPostMasReciente = new Post();
        segundoPostMasReciente.setEstado(EstadoContenido.APROBADO);
        String postId = postRepository.save(segundoPostMasReciente).block().getId();

        Pageable pageable = PageRequest.of(0, 2);

        Flux<Post> postsObtenidos = busquedaPostService.buscarTodos(pageable);

        StepVerifier
                .create(postsObtenidos)
                .assertNext(primerPostObtenido -> assertEquals(primerPostObtenido.getId(), postId))
                .expectComplete()
                .verify();
    }

    @Test
    public void cantidadPosts_postPendientesYAprobados_retornaAprobados() {

        Post unPost = new Post();
        unPost.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        postRepository.save(unPost).block().getId();

        Post segundoPostMasReciente = new Post();
        segundoPostMasReciente.setEstado(EstadoContenido.APROBADO);
        postRepository.save(segundoPostMasReciente).block().getId();

        Mono<Long> cantidadObtenida = busquedaPostService.cantidadPosts();

        StepVerifier
                .create(cantidadObtenida)
                .assertNext(cantidad -> assertEquals(cantidad, 1))
                .expectComplete()
                .verify();
    }

}
