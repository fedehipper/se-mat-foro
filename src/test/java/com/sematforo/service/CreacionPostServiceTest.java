package com.sematforo.service;

import com.sematforo.ApplicationTests;
import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.PostRepository;
import com.sematforo.vo.SolicitudPostVo;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class CreacionPostServiceTest extends ApplicationTests {

    @Autowired
    private CreacionPostService creacionPostService;

    @Autowired
    private PostRepository postRepository;

    @BeforeEach
    public void cleanDatabase() {
        postRepository.deleteAll().block();
    }

    @Test
    @WithUserDetails(value = "username")
    public void guardarPost_postValido_seGuardaElPost() {

        String titulo = " Titulo ";
        String contenido = " Dada una sucesión definida por la relación de recurrencia: 	$a_n - 3a_{n - 1} + 2a_{n - 2} = 0$ con $a_0 = 4$ y $a_1=7$ \n"
                + "\n"
                + "Halle una fórmula no recursiva que defina la misma sucesión.\n"
                + "\n"
                + "Demuestre por inducción completa que la fórmula no recursiva hallada es la correspondiente a la última sucesión. ";

        SolicitudPostVo solicitudPostVo = new SolicitudPostVo();
        solicitudPostVo.setTitulo(titulo);
        solicitudPostVo.setContenido(contenido);
        solicitudPostVo.setEtiquetas(List.of("trigo", "seno", "coseno"));

        Mono<Post> postGuardado = creacionPostService.guardarPost(solicitudPostVo);

        Usuario usuarioLogueado = (Usuario) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        StepVerifier
                .create(postGuardado)
                .assertNext(postObtenido -> {
                    assertThat(postObtenido.getContenido()).isEqualTo(solicitudPostVo.getContenido().trim());
                    assertThat(postObtenido.getTitulo()).isEqualTo(solicitudPostVo.getTitulo().trim());
                    assertThat(postObtenido.getUsuarioId()).isEqualTo(usuarioLogueado.getId());
                    assertThat(postObtenido.getEtiquetas()).isEqualTo(solicitudPostVo.getEtiquetas());
                    assertThat(postObtenido.getFechaHora()).isNotNull();
                    assertThat(postObtenido.getEstado()).isEqualTo(EstadoContenido.PENDIENTE_DE_APROBACION);
                })
                .expectComplete()
                .verify();
    }

}
