package com.sematforo.config;

import com.sematforo.domain.Rol;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.UsuarioRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class TestDataConfig implements ApplicationRunner {

    private final UsuarioRepository usuarioRepository;
    private final PasswordEncoder passwordEncoder;

    public TestDataConfig(UsuarioRepository usuarioRepository, PasswordEncoder passwordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) {
        usuarioRepository.save(usuario()).block();
        usuarioRepository.save(otroUsuario()).block();
        usuarioRepository.save(usuarioModerador()).block();
    }

    private Usuario otroUsuario() {
        Usuario usuario = usuarioBase("otroUsuario");
        usuario.setRol(Rol.USUARIO);
        return usuario;
    }

    private Usuario usuario() {
        Usuario usuario = usuarioBase("username");
        usuario.setRol(Rol.USUARIO);
        return usuario;
    }

    private Usuario usuarioBase(String username) {
        Usuario usuario = new Usuario();
        usuario.setEnabled(true);
        usuario.setUsername(username);
        usuario.setPassword(passwordEncoder.encode("una contrasenia"));
        usuario.setNombreCompleto("Nombre Completo");
        return usuario;
    }

    private Usuario usuarioModerador() {
        Usuario usuario = usuarioBase("moderador");
        usuario.setRol(Rol.MODERADOR);
        return usuario;
    }

}
