package com.sematforo.repository;

import com.sematforo.domain.Usuario;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface UsuarioRepository extends ReactiveMongoRepository<Usuario, String> {

    Mono<Usuario> findByUsername(String username);

    Mono<Boolean> existsByUsername(String username);

}
