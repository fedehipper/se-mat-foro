package com.sematforo.repository;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Respuesta;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface RespuestaRepository extends ReactiveMongoRepository<Respuesta, String> {

    Flux<Respuesta> findByPostIdAndEstado(String postId, EstadoContenido estadoContenido);

    Flux<Respuesta> findByEstado(EstadoContenido estadoContenido);

}
