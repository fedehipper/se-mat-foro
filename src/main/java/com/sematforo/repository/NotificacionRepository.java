package com.sematforo.repository;

import com.sematforo.domain.Notificacion;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface NotificacionRepository extends ReactiveMongoRepository<Notificacion, String> {

    Flux<Notificacion> findByUsuarioIdOrderByIdDesc(String usuarioId);
    
    Mono<Notificacion> findByIdAndUsuarioId(String notificacionId, String usuarioId);

    public Mono<Object> deleteById(Mono<Notificacion> notifiacionAEliminar);
    
    

}
