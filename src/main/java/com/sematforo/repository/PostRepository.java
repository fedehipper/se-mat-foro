package com.sematforo.repository;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface PostRepository extends ReactiveMongoRepository<Post, String> {

    Flux<Post> findByEstadoOrderByFechaHoraDesc(EstadoContenido estadoPost, Pageable pageable);
    
    Flux<Post> findByEstado(EstadoContenido estadoPost);
    
    Flux<Post> findByEstado(EstadoContenido estadoPost, Pageable pageable);

    Flux<Post> findByEtiquetasContaining(String etiqueta);

    Flux<Post> findByUsuarioIdOrderByFechaHoraDesc(String usuarioId, Pageable pageable);

    Flux<Post> findByUsuarioId(String usuarioId);

    Flux<Post> findByContenidoLikeIgnoreCaseAndEstado(String conceptoBuscado, EstadoContenido estadoPost);
    
    Flux<Post> findByIdAndEstado(String postId, EstadoContenido estadoPost);

}
