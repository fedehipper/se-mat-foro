package com.sematforo.repository;

import com.sematforo.domain.Etiqueta;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EtiquetaRepository extends ReactiveMongoRepository<Etiqueta, String> {

    Flux<Etiqueta> findByNombre(String nombre, Pageable pageable);
    
    Flux<Etiqueta> findByNombre(String nombre);
    
    Mono<Void> deleteByPostId(String postId);

}
