package com.sematforo.vo;

import com.sematforo.domain.EstadoContenido;

public class SolicitudModeracionPostVo {

    private String postId;
    private String motivoRechazo;
    private EstadoContenido estadoPost;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    public EstadoContenido getEstadoPost() {
        return estadoPost;
    }

    public void setEstadoPost(EstadoContenido estadoPost) {
        this.estadoPost = estadoPost;
    }

}
