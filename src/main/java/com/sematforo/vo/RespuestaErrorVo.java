package com.sematforo.vo;

public class RespuestaErrorVo {

    public RespuestaErrorVo(String mensaje) {
        this.mensaje = mensaje;
    }

    private String mensaje;

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
