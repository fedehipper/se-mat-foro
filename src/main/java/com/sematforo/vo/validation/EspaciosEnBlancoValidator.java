package com.sematforo.vo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EspaciosEnBlancoValidator implements ConstraintValidator<EspaciosEnBlancoValidation, String>  {

    @Override
    public boolean isValid(String nombreUsuario, ConstraintValidatorContext context) {
        return !nombreUsuario.contains(" ");
    }
    
}
