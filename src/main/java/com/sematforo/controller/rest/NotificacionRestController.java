package com.sematforo.controller.rest;

import com.sematforo.domain.Notificacion;
import com.sematforo.service.NotificacionService;
import com.sematforo.vo.NotificacionVo;
import com.sematforo.vo.SolicitudNotificacionVo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class NotificacionRestController {

    private final NotificacionService notificacionService;

    public NotificacionRestController(NotificacionService notificacionService) {
        this.notificacionService = notificacionService;
    }

    @PostMapping("/api/notificacion")
    public Mono<Notificacion> guardarModeracion(@RequestBody SolicitudNotificacionVo solicitudNotificacionVo) {
        return notificacionService.guardar(solicitudNotificacionVo);
    }

    @GetMapping("/api/notificaciones")
    public Flux<NotificacionVo> buscarTodas() {
        return notificacionService.buscarTodas();
    }

    @DeleteMapping("/api/notificaciones/{id}")
    public Mono<Void> eliminar(@PathVariable String id) {
        return notificacionService.eliminar(id);
    }

}
