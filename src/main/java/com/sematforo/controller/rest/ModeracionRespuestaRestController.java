package com.sematforo.controller.rest;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Respuesta;
import com.sematforo.service.ModeracionRespuestaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ModeracionRespuestaRestController {

    private final ModeracionRespuestaService moderacionRespuestaService;

    public ModeracionRespuestaRestController(ModeracionRespuestaService moderacionRespuestaService) {
        this.moderacionRespuestaService = moderacionRespuestaService;
    }

    @GetMapping("/api/respuestas/pendientes")
    public Flux<Respuesta> buscarRespuestasPendientes() {
        return moderacionRespuestaService.buscarTodas();
    }

    @PutMapping("/api/respuestas/{id}")
    public Mono<Respuesta> cambiarEstado(@PathVariable String id, @RequestParam EstadoContenido estado) {
        return moderacionRespuestaService.cambiarEstado(id, estado);
    }

}
