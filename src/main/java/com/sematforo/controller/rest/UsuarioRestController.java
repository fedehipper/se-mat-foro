package com.sematforo.controller.rest;

import com.sematforo.service.UsuarioService;
import com.sematforo.vo.UsuarioLogueadoVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class UsuarioRestController {

    private final UsuarioService usuarioService;

    public UsuarioRestController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping("/api/usuario/rol")
    public Mono<UsuarioLogueadoVo> buscarRolUsuario() {
        return usuarioService.buscarLogueado();
    }

}
