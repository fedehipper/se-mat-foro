package com.sematforo.controller.rest;

import com.sematforo.domain.Post;
import com.sematforo.service.BusquedaPostService;
import com.sematforo.service.CreacionPostService;
import com.sematforo.service.EdicionPostService;
import com.sematforo.vo.SolicitudPostVo;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class PostRestController {

    private final BusquedaPostService busquedaPostService;
    private final CreacionPostService creacionPostService;
    private final EdicionPostService edicionPostService;

    public PostRestController(BusquedaPostService busquedaPostService,
            CreacionPostService creacionPostService, EdicionPostService edicionPostService) {
        this.busquedaPostService = busquedaPostService;
        this.creacionPostService = creacionPostService;
        this.edicionPostService = edicionPostService;
    }

    @GetMapping("/api/posts")
    public Flux<Post> buscarTodos(@RequestParam int page, @RequestParam int size) {
        return busquedaPostService.buscarTodos(PageRequest.of(page, size));
    }

    @PostMapping("/api/post")
    public Mono<Post> crearPost(@RequestBody SolicitudPostVo postVo) {
        return creacionPostService.guardarPost(postVo);
    }

    @PostMapping("/api/post/{id}")
    public Mono<Post> editarPost(@PathVariable String id, @RequestBody SolicitudPostVo postVo) {
        return edicionPostService.editarPost(id, postVo);
    }

    @GetMapping("/api/posts/{id}")
    public Mono<Post> buscarPorId(@PathVariable String id) {
        return busquedaPostService.buscarPorId(id);
    }

    @GetMapping("/api/posts/cantidad")
    public Mono<Long> cantidadPosts() {
        return busquedaPostService.cantidadPosts();
    }

    @GetMapping("/api/posts/etiqueta/cantidad")
    public Mono<Long> cantidadPostsPorEtiqueta(@RequestParam String etiqueta) {
        return busquedaPostService.cantidadPostsPorEtiqueta(etiqueta);
    }

    @GetMapping("/api/posts/etiqueta/{etiqueta}")
    public Flux<Post> buscarPorEtiqueta(@PathVariable String etiqueta, @RequestParam int page, @RequestParam int size) {
        return busquedaPostService.buscarPorEtiqueta(etiqueta, PageRequest.of(page, size));
    }

}
