package com.sematforo.controller.rest;

import com.sematforo.domain.Post;
import com.sematforo.service.ModeracionPreguntaService;
import com.sematforo.vo.SolicitudModeracionPostVo;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ModeracionPreguntaRestController {

    private final ModeracionPreguntaService moderacionService;

    public ModeracionPreguntaRestController(ModeracionPreguntaService moderacionService) {
        this.moderacionService = moderacionService;
    }

    @GetMapping("/api/posts/pendientes")
    public Flux<Post> buscarTodos(@RequestParam int page, @RequestParam int size) {
        return moderacionService.buscarPostsPendientesAprobacion(PageRequest.of(page, size));
    }

    @GetMapping("/api/posts/pendientes/cantidad")
    public Mono<Long> cantidadPosts() {
        return moderacionService.cantidadPostsPendientesAprobacion();
    }

    @PutMapping("/api/posts/{postId}/moderacion")
    public Mono<Post> aprobar(@RequestBody SolicitudModeracionPostVo solicitudModeracionPostVo) {
        return moderacionService.cambiarEstado(solicitudModeracionPostVo);
    }

}
