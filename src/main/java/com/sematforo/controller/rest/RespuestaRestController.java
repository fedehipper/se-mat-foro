package com.sematforo.controller.rest;

import com.sematforo.domain.Respuesta;
import com.sematforo.service.RespuestaService;
import com.sematforo.vo.SolicitudRespuestaVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class RespuestaRestController {

    private final RespuestaService respuestaService;

    public RespuestaRestController(RespuestaService respuestaService) {
        this.respuestaService = respuestaService;
    }

    @PostMapping("/api/post/respuesta")
    public Mono<Respuesta> responder(@RequestBody SolicitudRespuestaVo solicitudRespuestaVo) {
        return respuestaService.guardar(solicitudRespuestaVo);
    }

    @GetMapping("/api/post/{id}/respuestas")
    public Flux<Respuesta> buscarParaPostId(@PathVariable String id) {
        return respuestaService.buscarParaPost(id);
    }

}
