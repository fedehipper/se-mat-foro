package com.sematforo.controller.rest;

import com.sematforo.domain.Post;
import com.sematforo.service.BusquedaMisPostsService;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class BusquedaMisPostsRestController {

    private final BusquedaMisPostsService busquedaMisPostsService;

    public BusquedaMisPostsRestController(BusquedaMisPostsService busquedaMisPostsService) {
        this.busquedaMisPostsService = busquedaMisPostsService;
    }

    @GetMapping("/api/usuario/posts")
    public Flux<Post> buscarTodos(@RequestParam int page, @RequestParam int size) {
        return busquedaMisPostsService.buscarTodos(PageRequest.of(page, size));
    }

    @GetMapping("/api/usuario/posts/cantidad")
    public Mono<Long> cantidadPosts() {
        return busquedaMisPostsService.cantidadMisPosts();
    }

}
