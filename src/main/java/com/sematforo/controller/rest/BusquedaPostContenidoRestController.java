package com.sematforo.controller.rest;

import com.sematforo.domain.Post;
import com.sematforo.service.BuscadorGeneralPostService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class BusquedaPostContenidoRestController {

    private final BuscadorGeneralPostService busquedaPostContenidoService;

    public BusquedaPostContenidoRestController(BuscadorGeneralPostService busquedaPostContenidoService) {
        this.busquedaPostContenidoService = busquedaPostContenidoService;
    }
    
    @GetMapping("/api/posts/contenido")
    public Flux<Post> buscarPorContenido(@RequestParam String busqueda, @RequestParam int page, @RequestParam int size) {
        return busquedaPostContenidoService.buscarPorContenido(busqueda, page, size);
    }

    @GetMapping("/api/posts/contenido/cantidad")
    public Mono<Long> cantidadPosts(@RequestParam String busqueda) {
        return busquedaPostContenidoService.cantidadPosts(busqueda);
    }

}
