package com.sematforo.controller.rest;

import com.sematforo.domain.Etiqueta;
import com.sematforo.service.EtiquetaService;
import com.sematforo.vo.SolicitudEtiquetaVo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class EtiquetaRestController {

    private final EtiquetaService etiquetaService;

    public EtiquetaRestController(EtiquetaService etiquetaService) {
        this.etiquetaService = etiquetaService;
    }

    @PostMapping("/api/etiquetas")
    public Flux<Etiqueta> guardar(@RequestBody Flux<SolicitudEtiquetaVo> solicitudEtiquetaVo) {
        return etiquetaService.guardar(solicitudEtiquetaVo);
    }
    
    @DeleteMapping("/api/etiquetas")
    public Mono<Void> eliminar(@RequestParam String postId) {
        return etiquetaService.eliminarTodas(postId);
    }

}
