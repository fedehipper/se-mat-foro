package com.sematforo.controller.rest.advice;

import com.sematforo.vo.RespuestaErrorVo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public RespuestaErrorVo handleCustomException(Exception exception) {
        return new RespuestaErrorVo(exception.getMessage());
    }

}
