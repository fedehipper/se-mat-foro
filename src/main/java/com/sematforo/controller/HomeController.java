package com.sematforo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping(value = {"/",
        "/creacion",
        "/posts/{id}",
        "/moderacion",
        "/moderacion/posts/{id}",
        "/post/success/{id}",
        "/edicion/{id}",
        "/mis-posts",
        "/posts/busqueda-contenido",
        "/posts/busqueda-etiqueta/{etiqueta}"})
    public String index() {
        return "index";
    }

}
