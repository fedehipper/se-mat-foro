package com.sematforo.controller;

import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.reactive.result.view.RedirectView;
import reactor.core.publisher.Mono;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/logout")
    public Mono<RedirectView> logout() {
        return ReactiveSecurityContextHolder
                .getContext()
                .map(contexto -> {
                    contexto.setAuthentication(null);
                    return contexto;
                })
                .map(contexto -> new RedirectView("/login?logout"));
    }

}
