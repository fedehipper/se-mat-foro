package com.sematforo.service;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Etiqueta;
import com.sematforo.domain.Post;
import com.sematforo.repository.EtiquetaRepository;
import com.sematforo.repository.PostRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class BusquedaPostService {

    private final PostRepository postRepository;
    private final EtiquetaRepository etiquetaRepository;

    public BusquedaPostService(PostRepository postRepository, EtiquetaRepository etiquetaRepository) {
        this.postRepository = postRepository;
        this.etiquetaRepository = etiquetaRepository;
    }

    public Flux<Post> buscarTodos(Pageable pageable) {
        return postRepository.findByEstadoOrderByFechaHoraDesc(EstadoContenido.APROBADO, pageable);
    }

    public Mono<Post> buscarPorId(String postGuardadoId) {
        return postRepository.findById(postGuardadoId);
    }

    public Mono<Long> cantidadPosts() {
        return postRepository
                .findByEstado(EstadoContenido.APROBADO)
                .count();
    }

    public Mono<Long> cantidadPostsPorEtiqueta(String etiqueta) {
        return etiquetaRepository
                .findByNombre(etiqueta)
                .map(Etiqueta::getPostId)
                .flatMap(postId -> postRepository.findByIdAndEstado(postId, EstadoContenido.APROBADO))
                .count();
    }
    
    public Flux<Post> buscarPorEtiqueta(String etiqueta, Pageable pageable) {
        return etiquetaRepository
                .findByNombre(etiqueta, pageable)
                .map(Etiqueta::getPostId)
                .flatMap(postId -> postRepository.findByIdAndEstado(postId, EstadoContenido.APROBADO));
    }

}
