package com.sematforo.service;

import com.sematforo.domain.Usuario;
import com.sematforo.repository.UsuarioRepository;
import com.sematforo.vo.UsuarioLogueadoVo;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UsuarioService implements ReactiveUserDetailsService {

    private final UsuarioRepository usuarioRepository;
    private final PasswordEncoder passwordEncoder;

    public UsuarioService(UsuarioRepository usuarioRepository, PasswordEncoder passwordEncoder) {
        this.usuarioRepository = usuarioRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return usuarioRepository
                .findByUsername(username)
                .cast(UserDetails.class);
    }

    public Mono<Usuario> cuentaConfirmada(String nombreUsuario, String usuarioId) {
        return usuarioRepository.findByUsername(nombreUsuario)
                .filter(usuario -> usuarioId.equals(usuario.getId()))
                .flatMap(usuario -> {
                    usuario.setEnabled(true);
                    return usuarioRepository.save(usuario);
                });
    }

    public Mono<Boolean> verificarExistenciaPorUsername(String username) {
        return usuarioRepository.existsByUsername(username);
    }

    public Mono<Boolean> validarNuevoPasswordEsIgualAlActual(String usuarioId, String nuevoPassword) {
        return usuarioRepository
                .findById(usuarioId)
                .map(usuario -> passwordEncoder.matches(nuevoPassword, usuario.getPassword()));
    }

    public Mono<UsuarioLogueadoVo> buscarLogueado() {
        return ReactiveSecurityContextHolder
                .getContext()
                .map(contexto -> ((Usuario) contexto.getAuthentication().getPrincipal()))
                .map(this::convertirAUsuarioLogueadoVo);
    }

    private UsuarioLogueadoVo convertirAUsuarioLogueadoVo(Usuario usuario) {
        UsuarioLogueadoVo usuarioLogueadoVo = new UsuarioLogueadoVo();
        usuarioLogueadoVo.setId(usuario.getId());
        usuarioLogueadoVo.setNombreCompleto(usuario.getNombreCompleto());
        usuarioLogueadoVo.setRol(usuario.getRol());
        return usuarioLogueadoVo;
    }

}
