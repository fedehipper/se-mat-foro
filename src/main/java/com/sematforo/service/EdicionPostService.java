package com.sematforo.service;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.PostRepository;
import com.sematforo.vo.SolicitudPostVo;
import java.time.LocalDateTime;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class EdicionPostService {

    private final PostRepository postRepository;
    private final ValidacionCreacionEdicionPostService validacionCreacionEdicionPostService;

    public EdicionPostService(PostRepository postRepository, ValidacionCreacionEdicionPostService validacionCreacionEdicionPostService) {
        this.postRepository = postRepository;
        this.validacionCreacionEdicionPostService = validacionCreacionEdicionPostService;
    }

    public Mono<Post> editarPost(String postId, SolicitudPostVo solicitudPostVo) {
        validacionCreacionEdicionPostService.validar(solicitudPostVo);
        return postRepository
                .findById(postId)
                .flatMap(post -> validarPostPerteneceAUsuarioLogueado(post, post.getUsuarioId()))
                .map(post -> modificarPost(post, solicitudPostVo))
                .flatMap(postRepository::save);
    }

    private Mono<Post> validarPostPerteneceAUsuarioLogueado(Post post, String usuarioCreadorPostId) {
        return ReactiveSecurityContextHolder
                .getContext()
                .map(contexto -> ((Usuario) contexto.getAuthentication().getPrincipal()).getId())
                .filter(usuarioLogueadoId -> usuarioLogueadoId.equals(usuarioCreadorPostId))
                .map(usuarioId -> post)
                .switchIfEmpty(Mono.error(new AccessDeniedException("Access is denied")));
    }

    private Post modificarPost(Post post, SolicitudPostVo solicitudPostVo) {
        post.setContenido(solicitudPostVo.getContenido().trim());
        post.setEtiquetas(solicitudPostVo.getEtiquetas());
        post.setFechaHora(LocalDateTime.now());
        post.setMotivoRechazo(null);
        post.setTitulo(solicitudPostVo.getTitulo().trim());
        post.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        return post;
    }

}
