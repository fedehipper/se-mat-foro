package com.sematforo.service;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.repository.PostRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class BuscadorGeneralPostService {

    private final PostRepository postRepository;

    public BuscadorGeneralPostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public Flux<Post> buscarPorContenido(String busqueda, int page, int size) {
        return buscarSinRepetirPorCoincidencias(busqueda)
                .skip(page * size)
                .take(size);
    }

    public Mono<Long> cantidadPosts(String busqueda) {
        return buscarSinRepetirPorCoincidencias(busqueda)
                .count();
    }

    private Flux<Post> buscarSinRepetirPorCoincidencias(String busqueda) {
        return Flux
                .just(busqueda.split(" "))
                .flatMap(texto -> postRepository.findByContenidoLikeIgnoreCaseAndEstado(texto, EstadoContenido.APROBADO))
                .distinct(Post::getId);
    }

}
