package com.sematforo.service;

import com.sematforo.vo.SolicitudPostVo;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class ValidacionCreacionEdicionPostService {

    private static final int CANTIDAD_MINIMA_CARACTERES_CONTENIDO_POST = 30;

    public void validar(SolicitudPostVo postVo) {
        Assert.notNull(postVo.getTitulo(), "El título es obligatorio.");
        Assert.isTrue(!postVo.getTitulo().isBlank(), "El título es obligatorio.");
        Assert.isTrue(postVo.getContenido().trim().length() > CANTIDAD_MINIMA_CARACTERES_CONTENIDO_POST,
                "Debes escribir tu consulta con un mínimo de " + CANTIDAD_MINIMA_CARACTERES_CONTENIDO_POST + " caracteres.");
        Assert.notEmpty(postVo.getEtiquetas(), "Debes incluir al menos una etiqueta que describa tu post.");
    }

}
