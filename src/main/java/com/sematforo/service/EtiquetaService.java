package com.sematforo.service;

import com.sematforo.domain.Etiqueta;
import com.sematforo.repository.EtiquetaRepository;
import com.sematforo.vo.SolicitudEtiquetaVo;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EtiquetaService {

    private final EtiquetaRepository etiquetaRepository;

    public EtiquetaService(EtiquetaRepository etiquetaRepository) {
        this.etiquetaRepository = etiquetaRepository;
    }

    public Flux<Etiqueta> guardar(Flux<SolicitudEtiquetaVo> solicitudesEtiquetaVo) {
        return solicitudesEtiquetaVo
                .map(this::crearEtiqueta)
                .flatMap(etiquetaRepository::save);
    }

    private Etiqueta crearEtiqueta(SolicitudEtiquetaVo solicitudEtiquetaVo) {
        Etiqueta etiqueta = new Etiqueta();
        etiqueta.setNombre(solicitudEtiquetaVo.getEtiqueta());
        etiqueta.setPostId(solicitudEtiquetaVo.getPostId());
        return etiqueta;
    }

    public Mono<Void> eliminarTodas(String postId) {
        return etiquetaRepository.deleteByPostId(postId);
    }

}
