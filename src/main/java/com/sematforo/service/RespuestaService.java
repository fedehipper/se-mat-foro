package com.sematforo.service;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Respuesta;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.RespuestaRepository;
import com.sematforo.vo.SolicitudRespuestaVo;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RespuestaService {

    private final RespuestaRepository respuestaRepository;

    public RespuestaService(RespuestaRepository respuestaRepository) {
        this.respuestaRepository = respuestaRepository;
    }

    public Mono<Respuesta> guardar(SolicitudRespuestaVo solicitudRespuestaVo) {
        return reactiveSecurityContext()
                .flatMap(context -> {
                    String usuarioId = (((Usuario) context.getAuthentication().getPrincipal()).getId());
                    Respuesta respuesta = crearRespuesta(solicitudRespuestaVo, usuarioId);
                    return respuestaRepository.save(respuesta);
                });
    }

    private Respuesta crearRespuesta(SolicitudRespuestaVo solicitudRespuestaVo, String usuarioId) {
        Respuesta respuesta = new Respuesta();
        respuesta.setPostId(solicitudRespuestaVo.getPostId());
        respuesta.setUsuarioId(usuarioId);
        respuesta.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        respuesta.setTexto(solicitudRespuestaVo.getRespuesta());
        return respuesta;
    }

    private Mono<SecurityContext> reactiveSecurityContext() {
        return ReactiveSecurityContextHolder
                .getContext();
    }
    
    public Flux<Respuesta> buscarParaPost(String postId) {
        return respuestaRepository.findByPostIdAndEstado(postId, EstadoContenido.APROBADO);
    }

}
