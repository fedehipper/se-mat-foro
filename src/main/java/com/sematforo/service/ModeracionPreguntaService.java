package com.sematforo.service;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.repository.PostRepository;
import com.sematforo.vo.SolicitudModeracionPostVo;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ModeracionPreguntaService {

    private final PostRepository postRepository;
    private final ValidacionModeracionService validacionModeracionService;

    public ModeracionPreguntaService(PostRepository postRepository, ValidacionModeracionService validacionModeracionService) {
        this.postRepository = postRepository;
        this.validacionModeracionService = validacionModeracionService;
    }

    @PreAuthorize("hasRole('MODERADOR')")
    public Flux<Post> buscarPostsPendientesAprobacion(Pageable pageable) {
        return postRepository.findByEstado(EstadoContenido.PENDIENTE_DE_APROBACION, pageable);
    }

    @PreAuthorize("hasRole('MODERADOR')")
    public Mono<Long> cantidadPostsPendientesAprobacion() {
        return postRepository
                .findByEstado(EstadoContenido.PENDIENTE_DE_APROBACION)
                .count();
    }

    @PreAuthorize("hasRole('MODERADOR')")
    public Mono<Post> cambiarEstado(SolicitudModeracionPostVo solicitudModeracionPostVo) {
        if (!EstadoContenido.APROBADO.equals(solicitudModeracionPostVo.getEstadoPost())) {
            validacionModeracionService.validarMotivoRechazo(solicitudModeracionPostVo.getMotivoRechazo());
        }
        return postRepository
                .findById(solicitudModeracionPostVo.getPostId())
                .flatMap(post -> {
                    post.setEstado(solicitudModeracionPostVo.getEstadoPost());
                    post.setMotivoRechazo(solicitudModeracionPostVo.getMotivoRechazo());
                    return postRepository.save(post);
                });
    }

}
