package com.sematforo.service;

import com.sematforo.domain.Post;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.PostRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class BusquedaMisPostsService {

    private final PostRepository postRepository;

    public BusquedaMisPostsService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public Flux<Post> buscarTodos(Pageable pageable) {
        return reactiveSecurityContext()
                .flatMapMany(context -> {
                    String usuarioId = (((Usuario) context.getAuthentication().getPrincipal()).getId());
                    return postRepository.findByUsuarioIdOrderByFechaHoraDesc(usuarioId, pageable);
                });
    }

    public Mono<Long> cantidadMisPosts() {
        return reactiveSecurityContext()
                .flatMap(context -> {
                    String usuarioId = (((Usuario) context.getAuthentication().getPrincipal()).getId());
                    return postRepository.findByUsuarioId(usuarioId).count();
                });
    }

    private Mono<SecurityContext> reactiveSecurityContext() {
        return ReactiveSecurityContextHolder
                .getContext();
    }

}
