package com.sematforo.service;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Respuesta;
import com.sematforo.repository.RespuestaRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ModeracionRespuestaService {

    private final RespuestaRepository respuestaRepository;

    public ModeracionRespuestaService(RespuestaRepository respuestaRepository) {
        this.respuestaRepository = respuestaRepository;
    }

    public Flux<Respuesta> buscarTodas() {
        return respuestaRepository.findByEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
    }

    public Mono<Respuesta> cambiarEstado(String respuestaId, EstadoContenido estadoContenido) {
        return respuestaRepository.findById(respuestaId)
                .flatMap(respuesta -> {
                    respuesta.setEstado(estadoContenido);
                    return respuestaRepository.save(respuesta);
                });
    }

}
