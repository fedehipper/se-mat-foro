package com.sematforo.service;

import com.sematforo.domain.Notificacion;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.NotificacionRepository;
import com.sematforo.vo.NotificacionVo;
import com.sematforo.vo.SolicitudNotificacionVo;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class NotificacionService {

    private final NotificacionRepository notificacionRepository;

    public NotificacionService(NotificacionRepository notificacionRepository) {
        this.notificacionRepository = notificacionRepository;
    }
    
    public Mono<Notificacion> guardar(SolicitudNotificacionVo solicitudNotificacionVo) {
        Notificacion notificacion = crearNotificacion(solicitudNotificacionVo);
        return notificacionRepository.save(notificacion);
    }

    private Notificacion crearNotificacion(SolicitudNotificacionVo solicitudNotificacionVo) {
        Notificacion notificacion = new Notificacion();
        notificacion.setTipoContenido(solicitudNotificacionVo.getTipoContenido());
        notificacion.setInfo(solicitudNotificacionVo.getInfo());
        notificacion.setContenidoId(solicitudNotificacionVo.getContenidoId());
        notificacion.setUsuarioId(solicitudNotificacionVo.getUsuarioId());
        notificacion.setTipo(solicitudNotificacionVo.getTipo());
        return notificacion;
    }

    public Flux<NotificacionVo> buscarTodas() {
        return reactiveSecurityContext()
                .flatMapMany(context -> {
                    String usuarioId = (((Usuario) context.getAuthentication().getPrincipal()).getId());
                    return notificacionRepository.findByUsuarioIdOrderByIdDesc(usuarioId);
                })
                .map(this::convertir);
    }

    private NotificacionVo convertir(Notificacion notificacion) {
        NotificacionVo notificacionVo = new NotificacionVo();
        notificacionVo.setId(notificacion.getId());
        notificacionVo.setInfo(notificacion.getInfo());
        notificacionVo.setPostId(notificacion.getContenidoId());
        notificacionVo.setUsuarioId(notificacion.getUsuarioId());
        notificacionVo.setTipo(notificacion.getTipo());
        notificacionVo.setMensaje(notificacion.getTipo().getMensaje());
        return notificacionVo;
    }

    private Mono<SecurityContext> reactiveSecurityContext() {
        return ReactiveSecurityContextHolder
                .getContext();
    }

    public Mono<Void> eliminar(String notificacionId) {
        return notificacionRepository.deleteById(notificacionId);
    }
}
