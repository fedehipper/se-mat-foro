package com.sematforo.service;

import com.sematforo.domain.EstadoContenido;
import com.sematforo.domain.Post;
import com.sematforo.domain.Usuario;
import com.sematforo.repository.PostRepository;
import com.sematforo.vo.SolicitudPostVo;
import java.time.LocalDateTime;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class CreacionPostService {

    private final PostRepository postRepository;
    private final ValidacionCreacionEdicionPostService validacionCreacionEdicionPostService;

    public CreacionPostService(PostRepository postRepository, ValidacionCreacionEdicionPostService validacionCreacionEdicionPostService) {
        this.postRepository = postRepository;
        this.validacionCreacionEdicionPostService = validacionCreacionEdicionPostService;
    }

    public Mono<Post> guardarPost(SolicitudPostVo solicitudPostVo) {
        validacionCreacionEdicionPostService.validar(solicitudPostVo);
        return crearPostParaUsuarioLogueado(solicitudPostVo).flatMap(postRepository::save);
    }

    private Mono<Post> crearPostParaUsuarioLogueado(SolicitudPostVo solicitudPostVo) {
        return reactiveSecurityContext()
                .map(context -> (((Usuario) context.getAuthentication().getPrincipal()).getId()))
                .map(usuarioId -> crearPost(solicitudPostVo, usuarioId));
    }

    private Mono<SecurityContext> reactiveSecurityContext() {
        return ReactiveSecurityContextHolder
                .getContext();
    }

    private Post crearPost(SolicitudPostVo solicitudPostVo, String usuarioId) {
        Post post = new Post();
        post.setTitulo(solicitudPostVo.getTitulo().trim());
        post.setContenido(solicitudPostVo.getContenido().trim());
        post.setFechaHora(LocalDateTime.now());
        post.setEtiquetas(solicitudPostVo.getEtiquetas());
        post.setUsuarioId(usuarioId);
        post.setEstado(EstadoContenido.PENDIENTE_DE_APROBACION);
        return post;
    }

}
