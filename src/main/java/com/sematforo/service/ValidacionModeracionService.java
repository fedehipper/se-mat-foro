package com.sematforo.service;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class ValidacionModeracionService {

    public void validarMotivoRechazo(String motivoRechazo) {
        Assert.notNull(motivoRechazo, "El motivo de rechazo es requerido.");
        Assert.isTrue(motivoRechazo.trim().length() > 60, "Debes escribir el motivo de rechazo con un mínimo de 60 caracteres.");
    }

}
