package com.sematforo.domain;

import org.springframework.data.annotation.Id;

public class Etiqueta {

    @Id
    private String id;
    private String nombre;
    private String postId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postsIds) {
        this.postId = postsIds;
    }

}
