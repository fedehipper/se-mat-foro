package com.sematforo.domain;

public enum TipoNotificacion {

    PREGUNTA_APROBADA("Tu pregunta fue aprobada"),
    PREGUNTA_RECHAZADA("Tu pregunta fue rechazada"),
    PREGUNTA_EN_REVISION("Estamos revisando tu pregunta"),
    RESPUESTA_EN_REVISION("Estamos revisando tu respuesta"),
    RESPUESTA_APROBADA("Tu respuesta fue aprobada"),
    RESPUESTA_RECHAZADA("Tu respuesta fue rechazada");

    private final String mensaje;

    TipoNotificacion(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

}
