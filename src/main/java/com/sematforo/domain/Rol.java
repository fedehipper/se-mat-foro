package com.sematforo.domain;

public enum Rol {

    USUARIO, MODERADOR
}
