package com.sematforo.domain;

public enum EstadoContenido {

    PENDIENTE_DE_APROBACION, APROBADO, RECHAZADO

}
