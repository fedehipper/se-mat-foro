# SE MAT FORO 
Aplicación foro para consultar ejercicios, o debatir sobre temas referentes a secundaria y universidad

### Íconos
https://react-icons.github.io/react-icons

### Front end creado en
https://es.reactjs.org/

### Backend con spring web flux
https://start.spring.io/

### React bootstrap
https://react-bootstrap.github.io/components/alerts

### React tag input
https://betterstack.dev/projects/react-tag-input/

### React tooltip
https://www.npmjs.com/package/react-tooltip

### React toastify
https://fkhadra.github.io/react-toastify/introduction/

### React social icons
https://github.com/jaketrent/react-social-icons


### Cómo levantar el proyecto

##Modificar las properties:

**spring.data.mongodb.uri**=mongodb+srv://sematforo:12345678Sematforo@cluster0.apivq.mongodb.net/test
**spring.data.mongodb.database**=sematforo

##Crear el document en la base:

{  "_id": {    "$oid": "62ba0f34e334e30312c9240c"  },  "username": "hipper",  "password": "$2a$12$oURmYceeMO.F3M2voy.2Hu.DV9qEwYR2diSZ2aREJJOSU4ci6wdxK",  "enabled": true,  "rol": "MODERADOR",  "nombreCompleto": "Federico Hipperdinger",  "_class": "com.sematforo.domain.Usuario"}
