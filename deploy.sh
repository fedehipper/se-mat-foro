cd /home/proyectos/se-mat-foro

echo "************************"
echo "       GIT PULL"
echo "************************"
git pull

echo "************************"
echo "         BUILD"
echo "************************"
mvn clean install -Dmaven.test.skip=true
cd /home/proyectos/se-mat-foro/target

echo "************************"
echo "         DEPLOY"
echo "************************"
java -jar se-mat-foro.jar
